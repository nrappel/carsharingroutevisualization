\chapter{Implementierung}
\label{Implementierung}
Im weiteren Verlauf wird die konkrete Umsetzung des in Kapitel \ref{Design} vorgestellten Designs beschrieben (vgl. Abbildung \ref{dataflow_implementation}). Hierbei werden \textit{konkrete Implementierungsschritte} und \textit{verwendete Technologiebausteine} bezüglich ihrer Relevanz erläutert.
\begin{figure}[htbp]
   \centering
   \includegraphics[width=0.9\textwidth]{images/3-Implementierung/dataflow_implementation}
   \caption[Modularer Aufbau der Implementierung]{Modularer Aufbau der Implementierung.}
   \label{dataflow_implementation} 
\end{figure}


\section{Crawling von Daten mit Hilfe einer Entwickler-API}
Der Carsharing-Anbieter \textit{Car2Go} ermöglicht Anwendungsentwicklern Datenzugriffe auf diverse Systeme. Beispielsweise können die aktuellen Positionskoordinaten nicht verwendeter Fahrzeuge in einer Stadt über eine \textit{für Anwendungsentwickler zugängliche API} ausgelesen werden. Entwickler sollen diese Daten in ihre Smartphone-, Tablet- oder Desktop-Anwendungen integrieren, um neue Kunden für das Konzept des Carsharing-Anbieters zu gewinnen. In diesem Unterkapitel wird der \textit{Funktionsumfang} der Programmierschnittstelle von \textit{Car2Go} beschrieben.

\subsection{Car2Go-Request}
\label{Car2Go-Request}
Ein \textit{Request (dt. Anfrage)} an die \textit{Car2Go-API} kann über die Generierung einer \textit{URL}\footnote{Englisch: \textit{Uniform Resource Locator}. Eine \textit{URL} bezeichnet einen Pfad oder ein Verzeichnis einer Website.} durchgeführt werden. Als Destination des Requests kann eine der Datenbankschnittstellen der Car2Go-Plattform ausgewählt werden. Die URL besteht, je nach Abfrageart, aus den in Tabelle \ref{car2go_request_elements} aufgeführten \textit{Bestandteilen}\footnote{Ausführbare URL-Beispiele sind im Anhang unter Tabelle \ref{car2go_api_examples} einsehbar.}. Um einen Request über die Car2Go-API durchführen zu können, müssen einige Zugriffsbestimmungen erfüllt werden. Allgemein werden in der Car2Go-API \textit{zwei Sicherheitsklassen} unterschieden:
\begin{description}
	\item[\textbf{Public-Access:}] Methoden, welche dieser Sicherheitskategorie zugeordnet sind, benötigen lediglich als zusätzlichen Parameter einen sogenannten \textit{Consumer-Key}. Ein Entwickler lässt sich diesen \textit{Consumer-Key} von Car2Go aushändigen und konkateniert ihn als String-Wert an die URL. Bei Methoden mit \textit{Public-Access} handelt es sich um Strukturen, welche \textit{keine} sensiblen, benutzerspezifischen Daten anfordern oder manipulieren können. 
	\item[\textbf{Protected-Access:}] Für Funktionen, welche als \textit{protected} markiert wurden, muss eine Authentifizierung über \textit{OAuth} vorgenommen werden. Bei \textit{OAuth} handelt es sich um ein Open-Source Protokoll, welches eine standardisierte, sichere API-Authentifizierung für Web-, Mobil- und Desktopanwendungen erlaubt \cite{oauth}. Ein Entwickler muss sich hierbei als Entwickler auf der Car2Go-Plattform registrieren und erhält anschließend die notwendigen Informationen für die Authentifizierung mittels \textit{OAuth}. Methoden, welche eine \textit{OAuth-Authentifizierung} erfordern, erlauben den Zugriff auf \textit{benutzerspezifische Konten- und Buchungsinformationen}.
\end{description}
\begin{table}[h]
\centering
\caption[Bestandteile einer exemplarischen API-Anfrage an den Car2Go-Server]{Bestandteile einer exemplarischen API-Anfrage an den Car2Go-Server.}
\begin{tabular}{lc}

\textbf{Server-Adresse}                & http://www.car2go.com/             \\ \hline
\textbf{API-Version (für Version 2.1)} & /api/v2.1/                         \\ \hline
\textbf{Car2Go-Methode}                & parkingspots/                      \\ \hline
\textbf{Stadt}                         & loc=Berlin/                        \\ \hline
\textbf{OAuth-Schlüssel}               & \&oauth\_consumer\_key=secret\_key \\
\end{tabular}
\label{car2go_request_elements}
\end{table}
Die Anfragen werden programmatisch über die Klasse \textbf{Car2GoWebCrawler.{\color{Fuchsia}java}} mittels des Interfaces \textbf{EndpointCommunication.{\color{Fuchsia}java}} (beide aus dem Package \textit{web\_crawler}) ausgelöst. Die resultierenden Anfrageergebnisse werden mithilfe der Modell-Klassen aus dem Package \textit{web\_crawler\_model} als Objekte (bzw. als Liste von Objekten) ausgegeben und können anschließend für automatisierte Analysen verwendet werden. Intern antwortet der Car2Go-Server \textit{JSON-Objekte} via \textit{HTTP-GET} auf einen Request. Diese Objekte werden ausgelesen und anschließend als Java-Objekt deklariert.

\subsection{API: Car2Go}
\label{car2go_api}
Die Car2Go-API stellt in \textit{Version 2.1} insgesamt \textit{11 Methoden} zur Verfügung. Tabelle \ref{car2go_api_table} beinhaltet alle Funktionen der Car2Go-API \cite{car2go}. Für die Analyse wird die Methode mit der URL \textit{http://www.car2go.com/api/v2.1/vehicles} \textit{periodisch} aufgerufen, um verfügbare Fahrzeuge zu ermitteln. Anhand zeitlich und geographisch abweichender Datensätze kann somit eine Fahrt simuliert werden. Da zum Zeitpunkt der Ausarbeitung kein Schlüssel für eine \textit{OAuth-Authentifizierung} seitens Car2Go(Daimler) vorlag, konnten keine Methoden mit OAuth-Authentifizierung genutzt werden\footnote{Folglich konnten in dieser Seminararbeit keine Analysen über das Buchungsverhalten im Carsharing-Bereich durchgeführt werden.}. 

\section{Datensicherung}
Im Hinblick auf die Datenvisualisierung mit der öffentlich zugänglichen JavaScript-Bibliothek \textit{Mapbox.js (vgl. Kapitel \ref{mapbox})} wurde eine \textit{PostgreSQL-Datenbank} für die Absicherung der Carsharing-Informationen verwendet\footnote{Mapbox ist über eine PostgreSQL-Schnittstelle kompatibel mit der Anwendungssoftware \textit{TileMill}. \textit{TileMill} erlaubt eine \textit{Offline-Betrachtung} von interaktiven Karten\cite{tilemill} und muss somit kein Kartenmaterial aus dem Internet beziehen.}. 

\subsection{Datenbankanbindung}
Eine \textit{gemeinsame Schnittstelle} zwischen der PostgreSQL-Datenbank, dem Analysebereich und der Car2Go-API wurde mittels der \textit{JDBC}\footnote{JDBC := Java Database Connectivity}-API von \textit{Oracle} implementiert \cite{jdbc}. \textit{JDBC} lässt sich als plattformunabhängige Lösung einbinden, beinhaltet eine Vielzahl unterschiedlicher Datenbanktreiber und erlaubt ein kurzfristiges Austauschen von Datenbankformaten. Die Klasse \textbf{DatabaseHandler.{\color{Fuchsia}java}} (aus dem gleichnamigen Package) enthält Methoden zum \textit{Aktualisieren}, \textit{Modifizieren} und \textit{Lesen} von Datensätzen der Datenbank.

\subsection{Datenimport}
Der \textit{Import} der Car2Go-Datensätze erfolgt mit dem Auslesen des \textit{GET-Requests} (siehe Kapitel \ref{Car2Go-Request}). Über die Methode \textit{updateCar2GoDatabase()} aus der Klasse \textbf{DatabaseHandler.{\color{Fuchsia}java}} werden nacheinander Instanzen der Klasse \textbf{State.{\color{Fuchsia}java}} erzeugt und diese abschließend in die Datenbank eingefügt. Beim Einfügen in die Datenbank werden zuerst die Entitäten \textit{car} und \textit{address} aktualisiert. Dies ist notwendig um keine Fehlermeldung bezüglich der Fremdschlüsselbeziehungen der Relation \textit{state} zu generieren. Der Aufrufgraph ist in Abbildung \ref{insert_car2go_datasets} dargestellt.
\begin{figure}[htbp]
   \centering
   \includegraphics[width=0.9\textwidth]{images/3-Implementierung/insert_car2go_datasets}
   \caption[Sequenzielle Durchführung der Datenbankaktualisierung]{Sequenzielle Durchführung der Datenbankaktualisierung.}
   \label{insert_car2go_datasets} 
\end{figure} 

\subsection{CSV-Export}
Um Daten in \textit{Tabellenkalkulationsprogramme} importieren zu können, wurden CSV-Export-Funktionalitäten in der Klasse \textbf{CsvHandler.{\color{Fuchsia}java}} bereitgestellt (siehe Package \textit{analysis}). Sowohl den Klassen zur Datenanalyse, als auch der DatabaseHandler-Klasse, wird ermöglicht CSV-Dateien zu schreiben oder zu lesen. Hierfür wurde die externe Bibliothek \textit{OpenCSV} eingebunden \cite{opencsv}. 


\section{Analyse- und Visualisierungsumgebung}
Im dritten Abschnitt dieses Kapitels wird die \textit{Analyse und Visualisierung der Daten} erläutert. Die Visualisierung wurde zum einen Teil mittels der \textit{API Mapbox.js} und zum anderen Teil mit Unterstützung der externen \textit{Web-Anwendung CartoDB} vorgenommen. Dieser Abschnitt beschreibt nachfolgend das implementierte Programmgerüst. Resultierende Analyse- und Visualisierungsergebnisse sind in Kapitel \ref{Datenanalyse} einzusehen.

\subsection{Konsoleninterface}
Verbindungen zur PostgreSQL-Datenbank können mit Methoden aus der Klasse \textbf{CarSharingAnalysisSuite.{\color{Fuchsia}java}} über \textit{Konsoleninteraktion} aufgebaut und unterbrochen werden. Eine Aktualisierung der Datenbank kann mittels parametrisierter Datumseingabe für gewünschte Zeiträume vorgenommen werden. Außerdem ist es möglich alle Analysemethoden anzusprechen und CSV-Exports auszulösen. Mittels der Funktion \textit{startUserConsoleInteraction()} wird die Interaktion über Konsole gestartet. 

\subsection{Web-Interface}
Um verteilt Funktionalitäten der API von Mapbox.js ansprechen zu können, wurde ein \textit{Tomcat-Server} in das Projekt eingebunden \cite{tomcat}. Dieser ermöglicht über die Klasse \textbf{ControllerServlet.{\color{Fuchsia}java}} aus dem Package \textit{controller} die Navigation durch das Web-Projekt. Der Tomcat-Server kann sowohl als \textit{Localhost}, als auch als \textit{Web-Server} konfiguriert werden. Als Grundlage einer dynamischen Erzeugung von HTML-Ausgaben wurden \textit{JavaServerPages (JSP)}\footnote{JSPs verhalten sich wie reguläre HTML-Seiten, ermöglichen darüber hinaus jedoch zudem Java-Code in HTML- oder XML-Seiten einzubetten.} verwendet, welche die Ausführung der \textit{JavaScript-Methoden} aus der Bibliothek Mapbox.js unterstützen. Alle \textit{JSP-Dateien} sind unter \textit{/WebContent/WEB-INF/jsp} einsehbar. 

\subsection{API: Mapbox.js}
\label{mapbox}
\begin{algorithm}
\caption{Exemplarischer Informationsfluss bei der Nutzung der Mapbox-API.}
\begin{algorithmic}
	\Require Internet-connection
	\State $mapboxConnectionStatus \gets$ establishMapboxConnection();\\
	 
	\If{( $mapboxConnectionStatus$ )}
    	\State $map \gets$ createAnalysisMap();
		\State $map$.placeDistrictsOnAnalysisMap();\\
		
		\State $vehicleData \gets$ getVehicleDataFromCsv();\\
		
		\If{( $!vehicleData.empty$ )}
			\ForAll{( $dataset$ : $vehicleData$ )} 
				\State $vehicleMarker \gets$ createVehicleMarker($dataset$);
				\State $map$.add($vehicleMarker$);
			\EndFor
		\EndIf
	 \EndIf
\end{algorithmic}
\label{mapbox_workflow}
\end{algorithm}
Bei \textit{Mapbox.js} handelt es sich um eine auf \textit{OpenStreetMap}\footnote{Bei OpenStreetMap handelt es sich um ein 2004 gegründetes internationales Projekt, mit dem Ziel, eine freie Weltkarte zu erschaffen \cite{openstreetmap}.} basierende API, welche die Erzeugung von interaktiven Karten ermöglicht. Mapbox erlaubt darüber hinaus, die Platzierung von \textit{Markierungen} (engl. Markers) auf einer zuvor erzeugten Karte. Eine Vielzahl an verfügbaren \textit{Karten-Ebenen} ermöglichen, je nach Bedarf, mehrere visuelle Sichten auf eine Karte. So können beispielsweise zusätzlich zu Straßenbezeichnungen auch Sehenswürdigkeiten dargestellt werden können. Mapbox.js basiert auf JavaScript und kann somit in allen gängigen, modernen Web-Browsern angezeigt werden. Die aktuelle API-Version von Mapbox.js ist v2.1.6.
Der in \textit{Algorithmus \ref{mapbox_workflow}} dargestellte Pseudocode beschreibt eine exemplarische Reihenfolge, in welcher die \textit{Mapbox-Bibliothek angesprochen werden kann}. Basierend auf einer bestehenden Internetverbindung, wird zunächst eine initiale Anfrage zum Austausch von Nutzer- und Metadaten an einen Mapbox-Server gestellt. Ist dieser Verbindungsaufbau erfolgreich, kann eine oder mehrere Karten von einem registrierten Mapbox-Konto geladen werden. Anschließend ist es möglich Operationen, wie beispielsweise das Positionieren von Markierungen auf der Karte, vorzunehmen.

\subsubsection{Initiale Verbindung}
Eine Verbindung zur \textit{Web-Service-API} von Mapbox kann mithilfe eines Zugriffsschlüssels (Access token) aufgebaut werden. Dieser Schlüssel wird einem \textit{L.mapbox-Objekt}, wie in Listing \ref{access_token_listing} gezeigt, zugewiesen. Einen Schlüssel kann man über ein Mapbox-Konto auf der Website von Mapbox generieren\footnote{Generierung des Schlüssels über die Mapbox-Homepage: https://www.mapbox.com/help/create-api-access-token/.}. Der Schlüssel erlaubt den Zugriff auf Kartenmaterial, welches auf einem der Mapbox-Servern abgelegt wurde. Generell können mehrere Schlüssel, von einem oder mehreren unterschiedlichen Konten verwendet werden.
\begin{lstlisting}[caption=Zugriff auf die API der Web-Services., label={access_token_listing}]
 L.mapbox.accessToken = '<your access token>';
\end{lstlisting}

\subsubsection{Erzeugung der Karte}
Das Generieren einer Karte ist über \textit{zwei Konstruktoren} möglich. Bei beiden muss die \textit{ID} des \textit{<div>-Tags} angegeben werden, in welches die Karte eingefügt werden soll. Optional kann im Konstruktor Kartenmaterial von einem Mapbox-Server geladen werden.\\
Über die Klasse \textit{L.mapbox.geocoder} kann man den initialen Betrachtungsbereich (Scope) der Karte festlegen. In der vorliegenden Implementierung ist eine Markierung innerhalb Berlins als initialer Kartenbereich angegeben (vgl. Listing \ref{create_map_listing}). 
\begin{lstlisting}[caption=Initiierung einer Karte., label={create_map_listing}]
// Erzeuge Karte in Element <div id="map_div_id">:
var map1 = L.mapbox.map('map_div_id');
// Erzeuge Karte mit Kartenmaterial aus dem Mapbox-Account: 
var map2 = L.mapbox.map('map_div_id', 'berlin.carsharing');
// Initiales Kartengebiet: Raum Berlin
var geo = L.mapbox.geocoder('mapbox.places');
geo.query('Berlin', callback);
\end{lstlisting}

\subsubsection{Stadtbezirke}
Optionale \textit{Kartenschichten (engl. Layer)} können über Ajax-Anfragen\footnote{Bei Ajax handelt es sich um eine auf jQuery basierende Möglichkeit asynchrone HTTP-Anfragen zu versenden \cite{ajax}.} in die Karte integriert werden. Dieses Vorgehen wurde auch beim Erzeugen einer Kartenschicht für Stadtbezirke gewählt. Mittels Ajax-Requests können intuitiv und leserlich GeoJSON-Dateien\footnote{Das \textit{GeoJSON-Format} kodiert eine Vielzahl an geographischen Datenstrukturen \cite{geojson}.} eingebunden werden. Mittels \textit{Ajax} kann, wie in Listing \ref{create_district_layer_listing} beschrieben, direkt auf die einzelnen Parameter der \textit{GeoJSON-Datei} zugegriffen werden. 
\begin{lstlisting}[caption=Stadtbezirke in Karte integrieren (GeoJSON-Format)., label={create_district_layer_listing}]
$.ajax({
	url: 'geojson_files/berliner-bezirke.geojson',
	dataType: 'json',
	success: function load(d) {
		// GeoJSON erfolgreich geladen...
	}
});

\end{lstlisting}

\subsubsection{Marker}
Um \textit{Markierungen (engl. Marker)} in eine Karte zu integrieren stellt die Mapbox-API die Klasse \textit{L.mapbox.marker} zur Verfügung. Über die Attribute der Objekte \textit{L.mapbox.marker.style} und \textit{L.mapbox.marker.icon} können Popup-Effekte, Farben, Inhalte und Größen einer Markierung festgelegt werden (siehe Listing \ref{create_marker_listing}).
\begin{lstlisting}[caption=Erzeugung einer Markierung (Markers)., label={create_marker_listing}]
var carIcon = L.mapbox.marker.icon({
		'marker-size': 'large',
		'marker-symbol': 'car',
		'marker-color': '#FF000'
});
\end{lstlisting} 

\subsubsection{Umwandlung: CSV-Daten zu Marker-Objekten}
Über die Bibliothek \textit{Leaflet.js}\footnote{Bei \textit{Leaflet.js} handelt es sich um eine Open-Source JavaScript-Bibliothek. \textit{Leaflet.js} beinhaltet eine Vielzahl an nützliche Funktionen, welche im Umgang mit interaktiven Karten verwendet werden können (\cite{leaflet}).} und deren API-Methode \textit{omnivore.csv()} können Daten als \textit{CSV-Datei} geladen und verarbeitet werden. Anschließend können die importierten Werte mittels der Methode \textit{addTo(L.mapbox.map)} zu einer Karte hinzugefügt werden. Erzeugte Markierungen werden zu \textit{GeoJSON-Objekten} konvertiert, um anschließend mittels des \textit{properties-Attributes} auf die CSV-Schlüssel und deren zugehörige Werte zuzugreifen. Listing \ref{omnivore_listing} zeigt einen exemplarischen Sourcecode (JavaScript). Die Schlüsselwerte \textit{latitude} und \textit{longitude} sind in der Leaflet-API \textit{vordefiniert} und werden automatisch als Längen- und Breitengrad für Fahrzeugpositionen verwendet.
\begin{lstlisting}[caption=Analysedaten mittels \textit{Leaflet.js} aus CSV-Dateien laden., label={omnivore_listing}]
omnivore.csv('path/to/csv')
	.on('ready', function(layer) {
		this.eachLayer(function(marker){
			var properties = marker.toGeoJSON().properties;
			var fuel = properties.fuel;
			var exteriorstate = properties.exteriorstate;
			// Marker-Transformation:
		})
	}		
).addTo(map);
\end{lstlisting} 
 
\subsubsection{Foursquare-Request}
Die Online-Plattform \textit{Foursquare} bietet eine riesige Ansammlung an Geschäften, Sehenswürdigkeiten oder sonstigen Orten. Jene Orte können von Nutzern bewertet oder kommentiert werden. Um Daten von Foursquare verwenden zu können, müssen diese über die \textit{Entwickler-Plattform} von Foursquare mittels Ajax-Request geladen werden \cite{foursquare_developer}. Voraussetzung hierfür ist die Erzeugung eines Entwickler-Kontos bei Foursquare, um eine gültige Anfrage mit \textit{Nutzer-ID und Passwort} stellen zu können. Listing \ref{foursquare_listing} zeigt eine exemplarische Anfrage an die Foursquare-Datenbank. Die Anfrage filtert Orte nach dem Schlagwort \textit{Tankstelle}, in der Stadt \textit{Berlin}, welche bereits vor dem \textit{01.01.2015} dem standortbezogenem Netzwerk bekannt waren.
\begin{lstlisting}[caption=Exemplarische Anfrage an die Foursquare-Plattform., label={foursquare_listing}]
var ID = 'foursquare_id';
var SECRET = 'foursquare_secretkey';
var API_ENDPOINT = 'https://api.foursquare.com/v2/venues/search' +
			  		'?client_id=CLIENT_ID' +
		  		  	'&client_secret=CLIENT_SECRET' +
		  		  	'&v=20150101' +
		  		  	'&near=Berlin' +
		  		  	'&query=Tankstelle' +
		  		  	'&callback=?';
var foursquarePlaces = L.layerGroup().addTo(map);
$.getJSON(API_ENDPOINT // AJAX-Request an Foursquare
	.replace('CLIENT_ID', ID)
	.replace('CLIENT_SECRET', SECRET), function(result, status) {
		if (status !== 'success') 
			return alert('Request to Foursquare failed');
		// Foursquare-Werte: result.response.venues[i]
});
\end{lstlisting} 

\subsection{CartoDB}
Bei \textit{CartoDB} handelt es sich um eine cloudbasierte SaaS-Lösung\footnote{SaaS := Software as a Service} zur Erzeugung von interaktiven Karten im Web-Browser \cite{cartodb}. \textit{CartoDB} wurde in der vorliegenden Seminararbeit für die Simulation der Positionsdaten verwendet.\\

Die Anwendung lässt sich in \textit{vier Bereiche} unterteilen: Die \textit{eigentliche Webanwendung}, eine \textit{Karten-API}, eine \textit{PostgreSQL-API} und eine \textit{Wrapper-Bibliothek} zur Integration von Karten- und SQL-API. Die Webanwendung ist als intuitive Weboberfläche gestaltet, welche ein übersichtliches Datenmanagement ermöglicht. Am Beginn eines CartoDB-Projektes werden zunächst über einen \textit{Upload von CSV-Dateien} die zu analysierenden Werte in eine Datenbank geladen. Neben dem \textit{Anzeigen} und \textit{Manipulieren} der Daten-Tabellen (vgl. Abbilung \ref{cartodb_data_table}) können die Daten bezüglich ihrer tatsächlichen Längen- und Breitengrade über die Betätigung einer \textit{einzelnen} Schaltfläche in einer Karte angezeigt werden. Über eine \textit{PostgreSQL-Schnittstelle} können Datensätze außerdem direkt in der Karte bearbeitet werden. Ein Abfragebeispiel ist im Anhang unter \ref{cartodb_postgresql_interface} einzusehen. Außerdem kann über den \textit{CartoCSS-Editor} das Erscheinungsbild des verwendeten Kartenmaterials mittels einer CSS-ähnlichen Skriptsprache modifiziert werden (siehe auch Abbildung \ref{cartodb_css_editor}).\\

Zur \textit{Datensimulation} muss in einen vom Datenmanagement getrennten Modus gewechselt werden\footnote{\textit{CartoDB} bezeichnet diesen Modus als \textit{Visualization-Mode}.}. Der \textit{Visualization-Mode} ermöglicht Simulationen anhand einer auszuwählenden Tabellenspalte (beispielsweise einer Spalte mit \textit{Uhrzeiten} - vgl. Abbildung \ref{cartodb_visualization}). Um die Position der Fahrzeuge an unterschiedlichen Zeitpunkten visualisieren zu können, wurde die Zeit der Erfassung eines Zustandes\footnote{Ein \textit{Zustand} beschreibt ein Fahrzeug, welches sich zu einem Zeitpunkt, an beliebigen Koordinaten befindet.} verwendet. Da \textit{CartoDB} für zeitbasierte Simulationen ein \textit{internes Datumsformat} benötigt, musste der Datentyp der Spalte \textit{time\_of\_documentation} der Relation \textit{state} auf den CartoDB-Datentyp \textit{cartodb\_date} abgeändert und alle zugehörigen Datensätze dem Format entsprechend angepasst werden.\\

\textit{CartoDB} stellt über \textit{zehn} verschiedene Möglichkeiten für Datensimulationen bereit.

