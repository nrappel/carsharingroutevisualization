\select@language {ngerman}
\contentsline {chapter}{Zusammenfassung}{i}{chapter*.3}
\contentsline {chapter}{Abstract}{ii}{chapter*.4}
\contentsline {chapter}{Inhaltsverzeichnis}{iii}{chapter*.4}
\contentsline {chapter}{Abbildungsverzeichnis}{v}{chapter*.6}
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Carsharing-Konzepte}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Round-Trip}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}One-Way}{2}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Free-Floating}{2}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Motivation}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Verwandte Arbeiten}{3}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Generierung von Simulationen und mathematischen Modellen}{3}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Untersuchung von Nachfrageverhalten im Free-Floating Carsharing}{3}{subsection.1.3.2}
\contentsline {chapter}{\numberline {2}Design}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Informationsmanagment}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Datenmodellierung}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}State}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Address}{6}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Car}{6}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Parking-Slot}{6}{subsection.2.2.4}
\contentsline {chapter}{\numberline {3}Implementierung}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Crawling von Daten mit Hilfe einer Entwickler-API}{7}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Car2Go-Request}{8}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}API: Car2Go}{9}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Datensicherung}{9}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Datenbankanbindung}{9}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Datenimport}{9}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}CSV-Export}{10}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Analyse- und Visualisierungsumgebung}{10}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Konsoleninterface}{10}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Web-Interface}{11}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}API: Mapbox.js}{11}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}Initiale Verbindung}{12}{subsubsection.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.3.2}Erzeugung der Karte}{12}{subsubsection.3.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3.3}Stadtbezirke}{13}{subsubsection.3.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.4}Marker}{13}{subsubsection.3.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.3.5}Umwandlung: CSV-Daten zu Marker-Objekten}{13}{subsubsection.3.3.3.5}
\contentsline {subsubsection}{\numberline {3.3.3.6}Foursquare-Request}{14}{subsubsection.3.3.3.6}
\contentsline {subsection}{\numberline {3.3.4}CartoDB}{15}{subsection.3.3.4}
\contentsline {chapter}{\numberline {4}Datenanalyse}{16}{chapter.4}
\contentsline {section}{\numberline {4.1}Formelle Definitionen}{16}{section.4.1}
\contentsline {section}{\numberline {4.2}Analyse der Nachfrage}{17}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Analysen zu unterschiedlichen Tageszeiten}{18}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Nachfrage in Berliner Stadtbezirken}{18}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Nachfrage an POIs}{19}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Nutzungsdauer und zur\IeC {\"u}ckgelegte Distanzen}{19}{section.4.3}
\contentsline {chapter}{\numberline {5}Zusammenfassung und Fazit}{20}{chapter.5}
\contentsline {section}{\numberline {5.1}L\IeC {\"o}sungsansatz der Seminararbeit}{20}{section.5.1}
\contentsline {section}{\numberline {5.2}Ergebnisse der Arbeit}{20}{section.5.2}
\contentsline {section}{\numberline {5.3}Kritik}{21}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Kurzer Analysezeitraum}{21}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Ungenaue Informationen \IeC {\"u}ber Abschluss einer Fahrt}{21}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Ausblick}{21}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Generierung gr\IeC {\"o}\IeC {\ss }erer Datenmengen}{21}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Ber\IeC {\"u}cksichtigung von Wartezeiten}{21}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Aufladestationen f\IeC {\"u}r E-Cars}{22}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Mobile Anwendung}{22}{subsection.5.4.4}
\contentsline {chapter}{Literaturverzeichnis}{I}{chapter*.19}
\contentsline {chapter}{Anhang}{1}{appendix*.20}
\contentsline {section}{\numberline {A.1}Tabellen}{1}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}Car2Go-API\FN@sf@gobble@opt {\textbf {Typen:} GET(G), POST(P), DELETE(D)}\FN@sf@gobble@opt {\textbf {Security:} public(+), private(-)}}{1}{subsection.A.1.1}
\contentsline {section}{\numberline {A.2}Repository}{2}{section.A.2}
\contentsline {section}{\numberline {A.3}Screenshots}{2}{section.A.3}
