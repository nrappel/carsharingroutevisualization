DROP TABLE address CASCADE;
DROP TABLE car CASCADE;
DROP TABLE parking_slot CASCADE;
DROP TABLE state CASCADE;


CREATE TABLE address (
 coordinates_x DOUBLE PRECISION NOT NULL,
 coordinates_y DOUBLE PRECISION NOT NULL,
 address_number NUMERIC NOT NULL,
 address_street VARCHAR(100) NOT NULL,
 address_plz CHAR(1000) NOT NULL,
 address_city VARCHAR(100),
 address_is_parking_slot BOOLEAN
);


CREATE TABLE car (
 car_plate VARCHAR(100) NOT NULL,
 car_vin VARCHAR(100),
 car_engine_type VARCHAR(100)
);


CREATE TABLE parking_slot (
 parking_slot_id SERIAL PRIMARY KEY,
 parking_slot_name VARCHAR(1000) NOT NULL,
 parking_slot_total_capacity NUMERIC,
 parking_slot_has_charging_pole BOOLEAN
);


CREATE TABLE state (
 coordinates_x DOUBLE PRECISION NOT NULL,
 coordinates_y DOUBLE PRECISION NOT NULL,
 car_plate VARCHAR(100) NOT NULL,
 time_of_documentation TIMESTAMP NOT NULL,
 car_fuel NUMERIC,
 car_interior_state VARCHAR(100),
 car_exterior_state VARCHAR(100),
 parking_slot_used_capacity NUMERIC
);


ALTER TABLE address ADD CONSTRAINT PK_address PRIMARY KEY (coordinates_x,coordinates_y);
ALTER TABLE car ADD CONSTRAINT PK_car PRIMARY KEY (car_plate);
ALTER TABLE parking_slot ADD CONSTRAINT PK_parking_slot PRIMARY KEY (parking_slot_id);
ALTER TABLE state ADD CONSTRAINT PK_state PRIMARY KEY (coordinates_x,coordinates_y,car_plate,time_of_documentation);

ALTER TABLE state ADD CONSTRAINT FK_state_0 FOREIGN KEY (coordinates_x,coordinates_y) REFERENCES address (coordinates_x,coordinates_y);
ALTER TABLE state ADD CONSTRAINT FK_state_1 FOREIGN KEY (car_plate) REFERENCES car (car_plate);
