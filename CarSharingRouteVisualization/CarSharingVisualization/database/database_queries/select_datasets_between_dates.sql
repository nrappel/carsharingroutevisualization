﻿SELECT * FROM "state" 
	WHERE "time_of_documentation" BETWEEN 
		to_timestamp('2015-03-02 00:00', 'YYYY-MM-DD HH24:MI')
		AND to_timestamp('2015-03-02 23:59', 'YYYY-MM-DD HH24:MI')