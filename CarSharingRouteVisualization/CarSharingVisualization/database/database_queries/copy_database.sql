﻿-- PostgreSQL-Query: Copy-Databse
--
-- This query creates a copy of the carsharing-database structure.
-- New-Database 	:= carsharingdb_copy
-- Database-to-copy 	:= carsharingdb
--

CREATE DATABASE carsharingdb_copy 

	WITH TEMPLATE carsharingdb 
	OWNER niklasrappel;