﻿-- PostgreSQL-Query: Different-Places
--
-- This Query returns several coordinates a car visits during
-- a set time-range and car plate. 
--

SELECT *
FROM (
	SELECT DISTINCT ON ("coordinates_x", "coordinates_y") *
	FROM "state"
	WHERE ("time_of_documentation"

		BETWEEN to_timestamp('2015-03-09 00:00','YYYY-MM-DD HH24:MI')
		AND 	to_timestamp('2015-03-09 23:59', 'YYYY-MM-DD HH24:MI'))

	AND "car_plate" LIKE 'B-GO2040'
) AS subquery
ORDER BY "time_of_documentation"
