

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="utilities.Tuple"%>
<%@page import="java.util.LinkedList"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
		<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js'></script>
		<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-pip/v0.0.2/leaflet-pip.js'></script>
		<script src='https://code.jquery.com/jquery-1.11.0.min.js'></script>
		<script> src="https://raw.github.com/tmcw/csv2geojson/gh-pages/csv2geojson.js"</script>
		<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
		<script src='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-omnivore/v0.2.0/leaflet-omnivore.min.js'></script>
		<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-image/v0.0.4/leaflet-image.js'></script>
		<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-draw/v0.2.2/leaflet.draw.js'></script>
		<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-geodesy/v0.1.0/leaflet-geodesy.js'></script>
		
		<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css' rel='stylesheet' />
		<link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-draw/v0.2.2/leaflet.draw.css' rel='stylesheet' />
		<link type="text/css" href="/CarSharingVisualization/CSS/district_map.css" rel="stylesheet" media="screen"></link>
	</head>

<body>
	<div id='map'></div>
	<div id='state' class='state'></div>
	<button id='snap' name="getDistrictPopulationButtonName" class='ui-button'>Print districts</button>
	
	<script>
		// SET ACCESS_TOKEN (Mapbox)
		L.mapbox.accessToken = 'pk.eyJ1IjoibnJhcHBlbCIsImEiOiJVa1h0WVljIn0.SYGvzm-95iyOf0CwF706Gw';

		var geocoder = L.mapbox.geocoder('mapbox.places');
		var state = document.getElementById('state');		
		var states = null;
		var map = L.mapbox.map('map', 'nrappel.lbpp2n8n');
		var myLayer = L.mapbox.featureLayer().addTo(map);
		
		geocoder.query('Berlin', showMap);

		var carPlate = '<%= request.getParameter("carPlate") %>';
		console.log('carPlate: ' + carPlate);
		//'B-GO2001';

		
		
		// ==========================
		// CREATE DISTRICTS:
		// Loads GeoJSON-File via URL
		//
		// ==========================		
			$.ajax({
		    url: 'geojson_files/berliner-bezirke.geojson',
		    dataType: 'json',
		    success: function load(d) {
		        states = L.geoJson(d).addTo(map);
		        L.marker([52.5, 13.3], {
		            icon: L.mapbox.marker.icon({
		                'marker-color': '#f86767'
		            }),
		            draggable: true
		        }).addTo(map)
		        .on('dragend', function(e) {
		            var layer = leafletPip.pointInLayer(this.getLatLng(), states, true);
		            if (layer.length) {
		              state.innerHTML = '<strong>' + layer[0].feature.properties.Name + '</strong>';
		            } else {
		              state.innerHTML = '';
		            }
		        });
		    }
		});
		
		
		var districtArray = [];
		
		// ==========================
		// CREATE VEHICLE-MARKERS:
		// Loads respective CSV-File via Omnivore (Leaflet.js)
		//
		// ==========================
			omnivore.csv('csv_files/csv_state_analysis/' + carPlate + '.csv').on('ready', function(layer) {
	       
	        this.eachLayer(function(marker) {
	        	var icon = L.mapbox.marker.icon({
	        		'marker-size': 'large',
	                'marker-symbol': 'car'
                });
	        		
	        	var fuelValue = marker.toGeoJSON().properties.fuel;
	            if (fuelValue > 70) {
	                icon = L.mapbox.marker.icon({
		        		'marker-size': 'large',
		                'marker-symbol': 'car',
		                'marker-color': '#90ee90'
	                });
	            } else if(fuelValue <= 70 && fuelValue > 29) {
	            	icon = L.mapbox.marker.icon({
		        		'marker-size': 'large',
		                'marker-symbol': 'car',
		                'marker-color': '#ff7f50'
	                });
	            } else if(fuelValue <= 29) {
	            	icon = L.mapbox.marker.icon({
		        		'marker-size': 'large',
		                'marker-symbol': 'car',
		                'marker-color': '#CC0000'
	                });
	            }
    	        marker.setIcon(icon);

	            var properties =  marker.toGeoJSON().properties;
	            var latLng = marker.getLatLng();

	            var myLayer = leafletPip.pointInLayer(latLng, states, true);
				
	            if(myLayer[0] !== null && myLayer[0] !== undefined && myLayer[0].feature !== null && myLayer[0].feature !== undefined){
		            districtArray.push(myLayer[0].feature.properties.Name);
		            
		            marker.bindPopup(	
		            		'<b>Plate:</b>\t' + carPlate + 
		            		'<br><b>Fuel:</b>\t' + properties.fuel +
		            		'<br><b>Date/Time:</b>\t' + properties.date + 
		            		'<br><b>Interior-State:</b>\t' + properties.interior_state +
		            		'<br><b>Exterior-State:</b>\t' + properties.exterior_state + 
		            		'<br><b>Parking-Slots:</b>\t' + properties.free_parking_slots +
		            		'<br><b>Latitude:</b>\t' + latLng.lat +
		            		'<br><b>Longitude:</b>\t' + latLng.lng + 
		            		'<br><b>District:</b>\t' + myLayer[0].feature.properties.Name);
	            };
	            
	        });
	    })
	    .addTo(map);
		
		
				
		// ==========================
		// DRAWING-FUNCTIONALITY:
		//
		// ==========================
		var featureGroup = L.featureGroup().addTo(map);

		var drawControl = new L.Control.Draw({
		  edit: {
		    featureGroup: featureGroup
		  },
		  draw: {
		    polygon: true,
		    polyline: false,
		    rectangle: false,
		    circle: false,
		    marker: false
		  }
		}).addTo(map);
		
		map.on('draw:created', showPolygonArea);
		map.on('draw:edited', showPolygonAreaEdited);
		
		function showPolygonAreaEdited(e) {
		  e.layers.eachLayer(function(layer) {
		    showPolygonArea({ layer: layer });
		  });
		}
		function showPolygonArea(e) {
		  featureGroup.clearLayers();
		  featureGroup.addLayer(e.layer);
		  e.layer.bindPopup((LGeo.area(e.layer) / 1000000).toFixed(2) + ' km<sup>2</sup>');
		  e.layer.openPopup();
		}
		
		
		
		
		// ==========================
		// CREATE SEVERAL LAYERS:
		// Uses L.mapbox.tileLayer; 
		//
		// ==========================
		var layers = {
		      Streets: L.mapbox.tileLayer('examples.map-i87786ca'),
		      Outdoors: L.mapbox.tileLayer('examples.ik7djhcc'),
		      Satellite: L.mapbox.tileLayer('examples.map-igb471ik')
		  };
		
		  layers.Streets.addTo(map);
		  L.control.layers(layers).addTo(map);
				
				
				
			
		function showMap(err, data) {
		    if (data.lbounds) {
		        map.fitBounds(data.lbounds);
		    } else if (data.latlng) {
		        map.setView([data.latlng[0], data.latlng[1]], 13);
		    }
		};
		
	</script>  
         
	</body>

</html>