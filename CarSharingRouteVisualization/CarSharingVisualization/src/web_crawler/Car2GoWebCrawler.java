package web_crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import web_crawler_model.Account;
import web_crawler_model.Booking;
import web_crawler_model.CanceledBooking;
import web_crawler_model.Coordinate;
import web_crawler_model.GasStation;
import web_crawler_model.Location;
import web_crawler_model.ParkingSpot;
import web_crawler_model.Position;
import web_crawler_model.Vehicle;

/**
 * This class provides methods to interact with the Car2Go-API.
 * Therefore a key is necessary, which has to be integrated into the URLs. 
 * 
 * @author 	Car2Go(Daimler), 
 * 			niklasrappel
 */
public class Car2GoWebCrawler implements EndpointCommunication {
	
	/**
	 * CAR2GO_KEY:
	 * This key is used to generate URLs for querying the Car2Go-API.
	 */
	private String car2GoWebKey = "car2gowebsite";
	
	/**
	 * DEFAULT_CONSTRUCTOR
	 */
	public Car2GoWebCrawler(){}
	
	/**
	 * 1st CONSTRUCTOR
	 * @param key
	 */
	public Car2GoWebCrawler(String key){
		this.setCar2GoWebKey(key);
	}
	

	
	/**Provides a list of all free car2go vehicles for a given location like Ulm or Austin.
	 * A OAuth Consumer Key is needed.
	 * http Request Type: public.
	 * 
	 * @author	car2go-dev, niklasrappel
	 * @param 	loc - Location e.g. ulm
	 * @param 	oauthConsumerKey - valid OAuth Consumer Key
	 * @return 	List<Vehicle> inner type web_crawler.objekts.Vehicle.java
	 * @see 	More information about KML can be found at: https://developers.google.com/kml/documentation/kmlreference?hl=de
	 */
	public List<Vehicle> getAllFreeVehicles(String loc, String oauthConsumerKey) {
		List<Vehicle> result = new ArrayList<Vehicle>();
		String url = String.format(URL_GETALLFREEVEHICLES, loc, this.car2GoWebKey);
		String data = getDataByURL(url);
		try {
			JSONObject jsonObj = new JSONObject(data);
			JSONArray jsonArray = jsonObj.getJSONArray(PLACEMARKS);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Position position = new Position(
						getCoordinatesFromJsonArray(jsonObject.getJSONArray((COORDINATES))),
						jsonObject.getString(ADRESS));
				Vehicle vehicle = new Vehicle(
						jsonObject.getString(VIN),
						jsonObject.getString(NAME),
						position,
						jsonObject.getString(EXTERIOR),
						jsonObject.getString(INTERIOR),
						jsonObject.getInt(FUEL)+"",
						jsonObject.getString(ENGINE_TYPE));
				result.add(vehicle);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	/**Provides a list of all free car2go vehicles for a given location like Ulm or Austin within a given Range in Kilometers of the submitted Position.
	 * A OAuth Consumer Key is needed.
	 * http Request Type: public.
	 * 
	 * @author	car2go-dev, niklasrappel
	 * @param 	loc - Location e.g. ulm
	 * @param 	oauthConsumerKey - valid OAuth Consumer Key
	 * @param 	range in km
	 * @param 	myPosition your current position
	 * @return 	List<Vehicle> inner type web_crawler.objekts.Vehicle.java
	 * @see 	More information about KML can be found at: https://developers.google.com/kml/documentation/kmlreference?hl=de
	 */
	public List<Vehicle> getAllFreeVehiclesInRange(String loc, String oauthConsumerKey, double range, Coordinate myPosition) {
		List<Vehicle> result = new ArrayList<Vehicle>();
		String url = String.format(URL_GETALLFREEVEHICLES, loc, oauthConsumerKey);
		String data = getDataByURL(url);
		try {
			JSONObject jsonObj = new JSONObject(data);
			JSONArray jsonArray = jsonObj.getJSONArray(PLACEMARKS);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Position position = new Position(
						getCoordinatesFromJsonArray(jsonObject.getJSONArray((COORDINATES))),
						jsonObject.getString(ADRESS));
				if (position.getDistKilometer(myPosition) <= range) {
					Vehicle vehicle = new Vehicle(
							jsonObject.getString(VIN),
							jsonObject.getString(NAME),
							position,
							jsonObject.getString(EXTERIOR),
							jsonObject.getString(INTERIOR),
							jsonObject.getString(FUEL),
							jsonObject.getString(ENGINE_TYPE));
					result.add(vehicle);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public List<ParkingSpot> getAllParkingSpots(String loc, String oauthConsumerKey) {
		List<ParkingSpot> result = new ArrayList<ParkingSpot>();
		String url = String.format(URL_GETALLPARKINGSPOTS, loc, oauthConsumerKey);
		String data = getDataByURL(url);
		try {
			JSONObject jsonObj = new JSONObject(data);
			JSONArray jsonArray = jsonObj.getJSONArray(PLACEMARKS);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Coordinate coordinate = new Coordinate(getCoordinatesFromJsonArray(jsonObject.getJSONArray(COORDINATES)));
				ParkingSpot parkingSpot = new ParkingSpot(
						coordinate,
						jsonObject.getString(NAME),
						jsonObject.getInt(TOTAL_CAPACITY),
						jsonObject.getInt(USED_CAPACITY),
						jsonObject.getBoolean(CHARGING_POLE));
				result.add(parkingSpot);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	

	/**
	 * This function splits a car2go-address in it´s components.
	 * A car2go-address contains:
	 * 	- The name of a street.
	 * 	- Number of the next house (belongs to street name).
	 * 	- The zipcode (shortened as 'PLZ')..
	 * 	- And the name of the respective city. 
	 * 
	 * A possible string-input:
	 * "Französische Straße 51, 10117 Berlin".
	 * 
	 * @author:		niklasrappel
	 * @param: 		carPositionString: A car2go address-string-input.
	 * @return:		HashMap<String, String> containing the following keys:
	 * 					- streetName
	 * 					- streetNumber
	 * 					- plz
	 * 					- city
	 */
	public static HashMap<String, String> parseCar2GoAddress(String carPositionString){
		if(null!=carPositionString){
			HashMap<String, String> addressHashMap = new HashMap<String, String>();
			
			String addressStreet = ""; String addressNumber = "";
			String addressPlz = ""; String addressCity = "";
			
			String[] streetNumberVsPlzCity = carPositionString.split(",");
			if(2==streetNumberVsPlzCity.length){
				String streetAndNumber = streetNumberVsPlzCity[0];
				String[] streetAndNumberArr = streetAndNumber.split(" ");
				
				int streetAndNumberArrLength = streetAndNumberArr.length;
				for(int index = 0; index < streetAndNumberArrLength-1; index++)
					addressStreet += streetAndNumberArr[index] + " ";
				
				addressNumber = streetAndNumberArr[streetAndNumberArrLength-1];
				
				if(addressStreet.contains("-"))
					addressStreet = addressStreet.replaceAll("\\s\\d+\\s-", "");
				
				addressStreet = addressStreet.trim();
				
				String plzAndCity = streetNumberVsPlzCity[1];
				String[] plzAndCityArr = plzAndCity.split(" ");
				
				addressPlz = plzAndCityArr[1];
				int plzAndCityArrLength = plzAndCityArr.length;
				for(int index = 2; index < plzAndCityArrLength; index++)
					addressCity += plzAndCityArr[index] + " ";
				
				addressCity = addressCity.trim();
			}
			
			if(addressStreet.isEmpty() || !isInteger(addressNumber)){
				addressStreet = addressNumber;
				addressNumber = "0";
			}
			
			if(addressPlz.isEmpty())
				addressPlz = "0";
			
			addressHashMap.put("streetName", addressStreet);
			addressHashMap.put("streetNumber", addressNumber);
			addressHashMap.put("plz", addressPlz);
			addressHashMap.put("city", addressCity);

			return addressHashMap;
		}
		return null;
	}
	
	
	/**
	 * This method proofs, if a string value
	 * could be converted to an integer. 
	 * 
	 * @param s:		String to proof.
	 * @return true, if string could be successfully converted to an integer. 
	 */
	public static boolean isInteger(String s) {
	    return isInteger(s,10);
	}

	
	private static boolean isInteger(String s, int radix) {
	    if(s.isEmpty()) return false;
	    for(int i = 0; i < s.length(); i++) {
	        if(i == 0 && s.charAt(i) == '-') {
	            if(s.length() == 1) return false;
	            else continue;
	        }
	        if(Character.digit(s.charAt(i),radix) < 0) return false;
	    }
	    return true;
	}
	
	
	/**Provides a list of car2go parking spots for a specific location like Ulm or Austin  within a given Range in Kilometers of the submitted Position.
	 * A OAuth Consumer Key is required.
	 * http Request Type: public.
	 * Can be provided as KML. (NOT YET IMPLEMENTED)
	 * @param loc - Location e.g. ulm
	 * @param oauth_consumer_key - valid OAuth Consumer Key
	 * @param range in km
	 * @param myPosition your current position
	 * @return List<ParkingSpot> inner type web_crawler.objekts.ParkingSpot.java
	 * @see More information about KML can be found at: https://developers.google.com/kml/documentation/kmlreference?hl=de
	 */
	public List<ParkingSpot> getAllParkingSpotsInRange(String loc, String oauth_consumer_key, double range, Coordinate myPosition) {
		List<ParkingSpot> result = new ArrayList<ParkingSpot>();
		String url = String.format(URL_GETALLPARKINGSPOTS, loc, oauth_consumer_key);
		String data = getDataByURL(url);
		try {
			JSONObject jsonObj = new JSONObject(data);
			JSONArray jsonArray = new JSONArray(jsonObj.getString(PLACEMARKS));
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Coordinate coordinate = new Coordinate(getCoordinatesFromJsonArray(jsonObject.getJSONArray(COORDINATES)));
				if (coordinate.getDistKilometer(myPosition) <= range) {
					ParkingSpot parkingSpot = new ParkingSpot(
							coordinate,
							jsonObject.getString(NAME),
							jsonObject.getInt(TOTAL_CAPACITY),
							jsonObject.getInt(USED_CAPACITY),
							jsonObject.getBoolean(CHARGING_POLE));
					result.add(parkingSpot);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	public List<Location> getAllLocations(String oauth_consumer_key) {
		List<Location> result = new ArrayList<Location>();
		String url = String.format(URL_GETALLLOCATIONS, oauth_consumer_key);
		String data = getDataByURL(url);
		try {
			JSONObject jsonObj = new JSONObject(data);
			JSONArray jsonArray = new JSONArray(jsonObj.getString(LOCATION));
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				JSONObject mapSection = jsonObject.getJSONObject(MAPS_SECTION);
				Location location = new Location(
						jsonObject.getString(COUNTRY_CODE),
						jsonObject.getString(DEFAULT_LANGUAGE),
						jsonObject.getString(LOCATION_ID), 
						jsonObject.getString(LOCATION_NAME), 
						new Coordinate(getCoordinatesFromJsonObjekt(mapSection.getJSONObject(CENTER))), 
						new Coordinate(getCoordinatesFromJsonObjekt(mapSection.getJSONObject(LOWER_RIGHT))), 
						new Coordinate(getCoordinatesFromJsonObjekt(mapSection.getJSONObject(UPPER_LEFT))), 
						jsonObject.get(TIMEZONE).toString());
				result.add(location);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	
	public List<GasStation> getAllPGasStations(String loc, String oauth_consumer_key) {
		List<GasStation> result = new ArrayList<GasStation>();
		String url = String.format(URL_GETALLGASSTATIONS, loc, oauth_consumer_key);
		String data = getDataByURL(url);
		try {
			JSONObject jsonObj = new JSONObject(data);
			JSONArray jsonArray = new JSONArray(jsonObj.getString(PLACEMARKS));
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Coordinate coordinate = new Coordinate(getCoordinatesFromJsonArray((JSONArray) jsonObject.get(COORDINATES)));
				GasStation gasStation = new GasStation(
						coordinate,
						jsonObject.getString(NAME));
				result.add(gasStation);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**Provides a list of car2go gas stations in context of specific location like Ulm or Austin within a given Range in Kilometers of the submitted Position.
	 * A OAuth Consumer Key is needed.
	 * http Request Type: public.
	 * Can be provided as KML. (NOT YET IMPLEMENTED)
	 * @param loc - Location e.g. ulm
	 * @param oauth_consumer_key - valid OAuth Consumer Key
	 * @param range in km
	 * @param myPosition your current position
	 * @return List<GasStation> inner type web_crawler.objekts.GasStation.java
	 * @see More information about KML can be found at: https://developers.google.com/kml/documentation/kmlreference?hl=de
	 */
	public List<GasStation> getAllPGasStationsInRange(String loc, String oauth_consumer_key, double range, Coordinate myPosition) {
		List<GasStation> result = new ArrayList<GasStation>();
		String url = String.format(URL_GETALLGASSTATIONS, loc, oauth_consumer_key);
		String data = getDataByURL(url);
		try {
			JSONObject jsonObj = new JSONObject(data);
			JSONArray jsonArray = new JSONArray(jsonObj.getString(PLACEMARKS));
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Coordinate coordinate = new Coordinate(getCoordinatesFromJsonArray((JSONArray) jsonObject.get(COORDINATES)));
				if (coordinate.getDistKilometer(myPosition) <= range) {
					GasStation gasStation = new GasStation(
							coordinate,
							jsonObject.getString(NAME));
					result.add(gasStation);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	
	/**
	 * This method returns data via URL.
	 * 
	 * @param url:	URL as string value.
	 * 
	 * @return: Result as string value
	 */
	private String getDataByURL(String url) {
		StringBuilder builder = new StringBuilder();
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}
	
	private Coordinate getCoordinatesFromJsonArray(JSONArray jsonArray) throws JSONException{
		Coordinate coordiante = new Coordinate(
				jsonArray.getDouble(1),
				jsonArray.getDouble(0)
		);
		
		return coordiante;
	}
	
	private Coordinate getCoordinatesFromJsonObjekt(JSONObject jsonObjekt) throws JSONException{
		Coordinate coordiante = new Coordinate(
				jsonObjekt.getDouble(LATITUDE), 
				jsonObjekt.getDouble((LONGITUDE)));
		return coordiante;
	}
	
	/**
	 * GETTER/SETTER
	 */

	@Override
	public List<Account> getAllAccounts(String loc) {
		return null;
	}

	@Override
	public List<Booking> getBookings(String loc) {
		return null;
	}

	@Override
	public Booking getBooking(String loc) {
		return null;
	}

	@Override
	public List<Booking> createBooking(String loc, String vin, String accountID) {
		return null;
	}

	@Override
	public CanceledBooking cancelBooking(String bookingID) {
		return null;
	}

	public String getCar2GoWebKey() {
		return car2GoWebKey;
	}

	public void setCar2GoWebKey(String car2GoWebKey) {
		this.car2GoWebKey = car2GoWebKey;
	}
}
