package analysis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import utilities.Tuple;
import utilities.Utilities;
import web_crawler_model.Trip;
import au.com.bytecode.opencsv.CSVWriter;
import database_model.State;


/**
 * This class contains the whole functionality for writing analysis results
 * as CSV-Files. Therefore a CSV-Analysis-File could be read and interpreted (if necessary). 
 * 
 * Functionality is supported by OpenCSV-Library. 
 * 
 * All methods are static referenced and could be called from remote
 * classes like this CsvHandler.<functionality>.
 * 
 * @author niklasrappel
 */
public class CsvHandler {

	/**
	 * CSV-OUTPUT-PATHES:
	 */
	public static final String PREFIX_PATH_TO_CSV_OUTPUT = "/Users/niklasrappel/Dropbox/UNI_WUE/SOSE_2015/git_repository/CarSharingRouteVisualization/CarSharingVisualization/WebContent/csv_files";
	public static final String PATH_CSV_CAR_USAGES_FOLDER = "/csv_car_usages_analysis/";
	public static final String PATH_CSV_NUMBER_OF_MOVEMENTS_FOLDER = "/csv_car_movements_analysis/";
	public static final String PATH_CSV_TIME_FOLDER = "/csv_car_time_analysis/";
	public static final String PATH_CSV_AVERAGE_TRIP_DISTANCE = "/csv_trip_distance_analysis/";
	public static final String PATH_CSV_AVERAGE_TRIP_DURATION = "/csv_trip_duration_analysis/";
	public static final String PATH_CSV_DISTRICTS_POPULATION_FOLDER = "/csv_district_car_population_analysis/";
	public static final String PATH_CSV_STATE_FOLDER = "/csv_state_analysis/";

	
	/**
	 * CSV-METHODS
	 */
	
	/**
	 * This method creates a CSV-File containing information about 
	 * all vehicles and their trip times.
	 * 
	 * CSV-Key-Structure: {"car_plate", "timestamp", "time_category"}
	 * 
	 * @param tripList:			Tuple of car and its respective trip list: ("B-GO-1234", {trip1,trip2})
	 * @param year:				Analysis Date (year)
	 * @param month:			Analysis Date (month)
	 * @param day:				Analysis Date (day)
	 */
	public static void writeListOfCarsPlatesAndTripTimeAsCsv(LinkedList<Tuple<String, LinkedList<Trip>>> tripList, int year, int month, int day){
		if(null!=tripList){
			try {
				File tmpFile = new File(PREFIX_PATH_TO_CSV_OUTPUT 
						+ PATH_CSV_TIME_FOLDER  
						+ month + day + year +".csv");

				final File parent_directory = tmpFile.getParentFile();
				if (null != parent_directory)
				parent_directory.mkdirs();
				
				CSVWriter writer = new CSVWriter(new FileWriter(tmpFile), ',');
				
    			String[] csvLegendValues = {"car_plate", "timestamp", "time_category"};
				writer.writeNext(csvLegendValues);

				for(Tuple<String, LinkedList<Trip>> carAndItsTripListTuple : tripList){
					for(Trip tmpTrip : carAndItsTripListTuple.getSecondValue()){
						Calendar cal = Calendar.getInstance();
						Date tmpDate = new Date(tmpTrip.getCircaTime().getTime());

						cal.setTime(tmpDate);
						int hour = cal.get(Calendar.HOUR_OF_DAY);
						int minute = cal.get(Calendar.MINUTE);
						
						String timeCategory = "";
						if(4==hour && 0==minute || 0<hour && hour<4 || 0==hour && 0!=minute)
							timeCategory="00:01-04:00";
						if(8==hour && 0==minute || 4<hour && hour<8 || 4==hour && 0!=minute)
							timeCategory="04:01-08:00";
						if(12==hour && 0==minute || 8<hour && hour<12 || 8==hour && 0!=minute)
							timeCategory="08:01-12:00";
						if(16==hour && 0==minute || 12<hour && hour<16 || 12==hour && 0!=minute)
							timeCategory="12:01-16:00";
						if(20==hour && 0==minute || 16<hour && hour<20 || 16==hour && 0!=minute)
							timeCategory="16:01-20:00";
						if(0==hour && 0==minute || 20<hour && hour<24 || 20==hour && 0!=minute)
							timeCategory="20:01-00:00";
						   
						String[] csvValue = {carAndItsTripListTuple.getFirstValue(), "" + tmpTrip.getCircaTime(), timeCategory};
						writer.writeNext(csvValue);
					}
				}
				
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * This method creates a CSV-File containing information about 
	 * all vehicles and their absolute number of movements on analyze-date.
	 * 
	 * CSV-Key-Structure: {"car_plate", "number_of_movements"}
	 * 
	 * @param tripList:			Tuple-list of cars and its number of movements. Exemplary list-element: ("B-GO-1234", 8)
	 * @param year:				Analysis Date (year)
	 * @param month:			Analysis Date (month)
	 * @param day:				Analysis Date (day)
	 */
	public static void writeListOfCarsPlatesAndNumberOfMovementsAsCsv(LinkedList<Tuple<String, Integer>> tupleListOfCarPlatesAndNumberOfMovements, int year, int month, int day){
		if(null!=tupleListOfCarPlatesAndNumberOfMovements){
			try {
				File tmpFile = new File(PREFIX_PATH_TO_CSV_OUTPUT 
						+ PATH_CSV_NUMBER_OF_MOVEMENTS_FOLDER  
						+ month + day + year +".csv");

				final File parent_directory = tmpFile.getParentFile();
				if (null != parent_directory)
				parent_directory.mkdirs();
				
				CSVWriter writer = new CSVWriter(new FileWriter(tmpFile), ',');
				
    			String[] csvLegendValues = {"car_plate", "number_of_movements"};
				writer.writeNext(csvLegendValues);

				for(Tuple<String, Integer> carAndItsNumberOfMovementsTuple : tupleListOfCarPlatesAndNumberOfMovements){
					String[] csvValue = {carAndItsNumberOfMovementsTuple.getFirstValue(), "" + carAndItsNumberOfMovementsTuple.getSecondValue()};
					writer.writeNext(csvValue);
				}
				
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * This method creates a CSV-File containing information about 
	 * all city districts and their respective vehicle-demand.
	 * 
	 * CSV-Key-Structure: {"district", "population"}
	 * 
	 * @param tripList:			Map<String, Integer> containing district-names and demand-values
	 * @param year:				Analysis Date (year)
	 * @param month:			Analysis Date (month)
	 * @param day:				Analysis Date (day)
	 */
	public static void writeCityDistrictsAndTheirPopulationAsCsv(Map<String, Integer> mymap, int year, int month, int day){
		if(null!=mymap){
			try {
				File tmpFile = new File(PREFIX_PATH_TO_CSV_OUTPUT 
						+ PATH_CSV_DISTRICTS_POPULATION_FOLDER  
						+ month + day + year +".csv");

				final File parent_directory = tmpFile.getParentFile();
				if (null != parent_directory)
					parent_directory.mkdirs();
				
				CSVWriter writer = new CSVWriter(new FileWriter(tmpFile), ',');
				
    			String[] csvLegendValues = {"district", "population"};
				writer.writeNext(csvLegendValues);

				Iterator<Entry<String, Integer>> it = mymap.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>)it.next();
			        System.out.println(pair.getKey() + " = " + pair.getValue());
			        String[] csvValue = {pair.getKey(), "" + pair.getValue()};
					writer.writeNext(csvValue);
			        it.remove(); 
			    }
				
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	
	/**
	 * This method creates a CSV-File containing information about 
	 * all analyzed states on a respective date. 
	 * 
	 * CSV-Key-Structure: {"latitude", "longitude", "car_plate", "date", "cartodb_date", "fuel", "interior_state", "exterior_state", "free_parking_slots"}
	 * 
	 * Calls method 'createCsvLineForStateAnalysisValues()' for creating a CSV-Value
	 * 
	 * @param analysisFactory:			Analysis-Factory, that contains all analysis-methods. 
	 */
	public static void writeStatesFromAllCarsAndItsStatesAsCsv(CarSharingAnalysisMethods analysisFactory){
    	LinkedList<Tuple<String, LinkedList<State>>> listOfCarsAndItsStates = analysisFactory.createTupleListOfAllCarsAndItsStatesByDate();	
    	File statesOfAllCars = new File(	PREFIX_PATH_TO_CSV_OUTPUT 
											  + PATH_CSV_STATE_FOLDER 
											  + analysisFactory.getAnalysisMonth() + analysisFactory.getAnalysisDay() + analysisFactory.getAnalysisYear() + ".csv");
    		
		String[] csvLegendValues = {"latitude", "longitude", "car_plate", "date", "cartodb_date", "fuel", "interior_state", "exterior_state", "free_parking_slots"};
    	try {
	    	CSVWriter statesOfAllCarsCsvWriter = new CSVWriter(new FileWriter(statesOfAllCars), ',');
	    	statesOfAllCarsCsvWriter.writeNext(csvLegendValues);
	    		
	    	for(Tuple<String, LinkedList<State>> tmpCarAndItsStateTuple : listOfCarsAndItsStates){    					
	        	File tmpFile = new File(PREFIX_PATH_TO_CSV_OUTPUT 
				       					+ PATH_CSV_STATE_FOLDER  
				       					+ tmpCarAndItsStateTuple.getFirstValue() +".csv");
	        			
	        	final File parent_directory = tmpFile.getParentFile();
	        	if (null != parent_directory)
	        	    parent_directory.mkdirs();
	        		
	        	CSVWriter writer = new CSVWriter(new FileWriter(tmpFile), ',');
	        	writer.writeNext(csvLegendValues);
	        			
	        	for(State tmpState : tmpCarAndItsStateTuple.getSecondValue()){
	        		String cartodbDate = Utilities.getCartoDbDateByState(tmpState);
	        		
					String[] csvValue = createCsvLineForStateAnalysisValues(tmpCarAndItsStateTuple, tmpState, cartodbDate);
	        		writer.writeNext(csvValue);
	        		statesOfAllCarsCsvWriter.writeNext(csvValue);
	        	}
				writer.close();
					
	    	}
	    	statesOfAllCarsCsvWriter.close();
	   		
    	} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * This method creates a CSV-File containing information about 
	 * all vehicles average trip distances.
	 * 
	 * CSV-Key-Structure: {"car_plate", "average_trip_distance"}
	 * 
	 * @param averageTrips:		Number of average trips (double value)
	 * @param year:				Analysis Date (year)
	 * @param month:			Analysis Date (month)
	 * @param day:				Analysis Date (day)
	 */
	public static void writeAverageTripDistanceAsCsv(double averageTrips, int year, int month, int day){
		if(0<=averageTrips){
			try {
				File tmpFile = new File(PREFIX_PATH_TO_CSV_OUTPUT 
						+ PATH_CSV_AVERAGE_TRIP_DISTANCE  
						+ month + day + year +".csv");

				final File parent_directory = tmpFile.getParentFile();
				if (null != parent_directory)
				parent_directory.mkdirs();
				
				CSVWriter writer = new CSVWriter(new FileWriter(tmpFile), ',');
				
    			String[] csvLegendValues = {"car_plate", "average_trip_distance"};
				
    			writer.writeNext(csvLegendValues);
    			writer.writeNext(
    					new String[]{"ALL_CARS", ""+averageTrips}
    			);
				
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * This method creates a CSV-File containing information about 
	 * all vehicles average trip duration.
	 * 
	 * CSV-Key-Structure: {"car_plate", "average_trip_duration"}
	 * 
	 * @param averageTrips:		Average trip duration (double value)
	 * @param year:				Analysis Date (year)
	 * @param month:			Analysis Date (month)
	 * @param day:				Analysis Date (day)
	 */
	public static void writeAverageTripDurationAsCsv(double averageTrips, int year, int month, int day){
		if(0<=averageTrips){
			try {
				File tmpFile = new File(PREFIX_PATH_TO_CSV_OUTPUT 
						+ PATH_CSV_AVERAGE_TRIP_DURATION  
						+ month + day + year +".csv");

				final File parent_directory = tmpFile.getParentFile();
				if (null != parent_directory)
				parent_directory.mkdirs();
				
				CSVWriter writer = new CSVWriter(new FileWriter(tmpFile), ',');
				
    			String[] csvLegendValues = {"car_plate", "average_trip_duration"};
				
    			writer.writeNext(csvLegendValues);
    			writer.writeNext(
    					new String[]{"ALL_CARS", ""+averageTrips}
    			);
				
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * This method writes a CSV-Value depending on a respective state-information
	 * and is called by method 'writeStatesFromAllCarsAndItsStatesAsCsv()'.
	 * 
	 * 
	 * @param tmpCarAndItsStateTuple:		Tuple of a car and it´s state list.
	 * @param tmpState:						The current, written state.
	 * @param cartodbDate:					CartoDB-Date is used for simulations via CartoDB (uses method: 'getCartoDbDateByState' for each state!).
	 * @return
	 */
	private static String[] createCsvLineForStateAnalysisValues(Tuple<String, LinkedList<State>> tmpCarAndItsStateTuple, State tmpState, String cartodbDate) {
		String[] csvValue = {
				"" + tmpState.getStatesLatitudeCoordinates(),
				"" + tmpState.getStatesLongitudeCoordinates(),
				tmpCarAndItsStateTuple.getFirstValue(),
				new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(tmpState.getStatesTimestamp()),
				cartodbDate,
				"" + tmpState.getStatesCarFuel(),
				tmpState.getStatesCarInteriorState(),
				tmpState.getStatesCarExteriorState(),
				"" + tmpState.getStatesParkingSlotsUsed()
		};
		return csvValue;
	}


	/**
	 * This method creates a PostgreSQL-Query via Utilities.createPostgreSQLCsvOutputPathString and stores
	 * query-results as CSV-File.
	 *  
	 * @param psqlConnection:		PostgreSQL-Database connection
	 * @param tableName:			Database-Relation(Table) which should be stored
	 * @param columnArr:			String-Array that contains table-id´s which should be stored in the CSV-File
	 * @param outputPath:			The output-path where the CSV-File should be stored on clients-system (e.g. /System/Users/root/)
	 */
	public static void writeDataFromPostgreSQLDatabaseAsCsv(Connection psqlConnection, String tableName, String[] columnArr, String outputPath){
    	if(null!=tableName && null!=outputPath){
			try {
				String columnIdentifiers = "*";
				if(null!=columnArr && columnArr.length>0){
					columnIdentifiers = "";
					for(String tmpColumn : columnArr)
						columnIdentifiers += tmpColumn;
					
					columnIdentifiers = columnIdentifiers.substring(0, columnIdentifiers.length()-1);
				}
	        	
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy_HH:mm");
				
				String csvExportQuery = 
						Utilities.createPostgreSQLCsvOutputPathString(tableName, outputPath, columnIdentifiers, timestamp, sdf);
				
				PreparedStatement selectExistingCarPlatesInDatabase = psqlConnection.prepareStatement(csvExportQuery);
				selectExistingCarPlatesInDatabase.execute();

			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    }
}