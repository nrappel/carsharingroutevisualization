package analysis;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.Period;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import utilities.Tuple;
import utilities.Utilities;
import web_crawler_model.Coordinate;
import web_crawler_model.Trip;
import database_handler.DatabaseHandler;
import database_model.State;

/**
 * CarSharingAnalysisMethods.java contains methods, that contains analysis context. 
 * All methods could be accessed by an CarSharingAnalysisMethods-object, 
 * which is created using a PostgreSQL-connection and date values. 
 * 
 * The class contains the following methods:
 * 		-Getter/Setter PostgreSQL-Connection: 
 * 		 this.getPsqlConnection(), this.setPsqlConnection(Connection psqlConnection)
 * 		
 * 		-Method for testing if a date is valid (returns true if date is valid): 
 * 		 this.isValidDate(int year, int month, int day) 
 * 
 * @author niklasrappel
 *
 */
public class CarSharingAnalysisMethods {

	/**
	 * PostgreSQL-Connection Object:
	 */
	private Connection psqlConnection;
	
	/**
	 * Analysis-Date:
	 */
	private int analysisYear;
	private int analysisMonth;
	private int analysisDay;
	
	/**
	 * Flag: Write as CSV-file:
	 */
	private boolean writeAsCsv;
	
	private static final String ERROR_MESSAGE_INVALID_DATE = "ERROR: Invalide date!\n"
			+ "Param year has to be >2000; "
			+ "Param month has to be >0 and <13 (January - December); "
			+ "Param day has to be >0 and <32; "
			+ "--> Analysis interrupted!";

	
	/**
	 * Database-Connection-Constructor
	 * @param psqlConnection
	 */
	public CarSharingAnalysisMethods(Connection psqlConnection){
		this.psqlConnection = psqlConnection;
	}
	
	/**
	 * Database-Connection-Constructor (using date)
	 * 
	 * @param psqlConnection
	 * @param year
	 * @param month
	 * @param day
	 * @param writeAsCsv
	 */
	public CarSharingAnalysisMethods(Connection psqlConnection, int year, int month, int day, boolean writeAsCsv){
		if(Utilities.isValidDate(year, month, day)){
			this.psqlConnection = psqlConnection;
			this.analysisYear = year;
			this.analysisMonth = month;
			this.analysisDay = day;
			this.writeAsCsv = writeAsCsv;
		} else{
			System.err.append(ERROR_MESSAGE_INVALID_DATE);
		}
		
	}
	
	
	/**
	 * GETTER / SETTER
	 * 
	 */
	public Connection getPsqlConnection() {
		return psqlConnection;
	}

	public void setPsqlConnection(Connection psqlConnection) {
		this.psqlConnection = psqlConnection;
	}
	
	public int getAnalysisYear() {
		return analysisYear;
	}

	public int getAnalysisMonth() {
		return analysisMonth;
	}

	public int getAnalysisDay() {
		return analysisDay;
	}

	public boolean isWriteAsCsv() {
		return writeAsCsv;
	}

	public static String getErrorMessageInvalidDate() {
		return ERROR_MESSAGE_INVALID_DATE;
	}

	public void setAnalysisYear(int analysisYear) {
		this.analysisYear = analysisYear;
	}

	public void setAnalysisMonth(int analysisMonth) {
		this.analysisMonth = analysisMonth;
	}

	public void setAnalysisDay(int analysisDay) {
		this.analysisDay = analysisDay;
	}

	public void setWriteAsCsv(boolean writeAsCsv) {
		this.writeAsCsv = writeAsCsv;
	}
	
	
	/**
	 * ANALYSIS_METHODS:
	 * ===========================================================================
	 * 
	 */
	
	
	/**
	 * This function creates a tuple list of cars and their respective trips.
	 * Example output: {(B-GO-1234, {trip1, trip2}), (B-GO-5678, {trip3, trip4})}
	 * 
	 * @return: LinkedList<Tuple<String, LinkedList<Trip>>>
	 */
	public LinkedList<Tuple<String, LinkedList<Trip>>> getCarsAndItsTripsByDate(){
		LinkedList<Tuple<String, LinkedList<Trip>>> tripList = createTupleListOfAllCarsAndItsTripsByDate();
		if(this.writeAsCsv)
			CsvHandler.writeListOfCarsPlatesAndTripTimeAsCsv(
					tripList, 
					this.analysisYear, 
					this.analysisMonth, 
					this.analysisDay
			);
				
		return tripList;
	}
	
	
	/**
	 * This function returns the car-population in several city districts.
	 * Example: Berlin-Mitte: 1134, Pankow: 224, ...
	 * 
	 * @param request: 		Http-Request for redirecting pages; 
	 */
	public static void getCarPopulationOfCityDistricts(HttpServletRequest request) {
		try {
			Gson gson = new Gson();	
			String jsonString = new String(); 
		   
			String line = "";
		    BufferedReader reader = request.getReader();
		    
		    while ((line = reader.readLine()) != null)
		      jsonString += line;
		    
		    Map<String,Integer> map = new HashMap<String,Integer>();
		    
		    try{
		    	@SuppressWarnings("unchecked")
				Map<String, Integer> mymap = gson.fromJson(jsonString, map.getClass());
			    CsvHandler.writeCityDistrictsAndTheirPopulationAsCsv(mymap, 2015, 3, 9);
		    } 
		    catch(JsonSyntaxException jsonException){
		    	jsonException.printStackTrace();
		    }
		    catch(IllegalStateException stateException){
		    	stateException.printStackTrace();
		    }			  
		} catch (Exception e) { 
		    e.printStackTrace();
		}
	}
	
	
	/**
	 * This method calculates the average trip distance by date. 
	 * Therefore it uses trip-objects to get the trip distances and divides it through the absolut
	 * number of trips.
	 * 
	 * Example: 
	 * 	- 5 Trips {4km, 10km, 12km, 18km, 16km}: 	60km absolut
	 * 	- Average trip distance: 					60km / 5trips = 12km
	 * 
	 * @return: Average trip distance, as a double value.
	 */
	public double getAverageTripDistanceByDate(){
		LinkedList<Trip> completeTripList = createListOfAllTripsByDate();
			
		int numberOfTrips = completeTripList.size();
		double averageTripDistanceValue = 0;
		double completeTripDistance = 0;
			
		for(Trip tmpTrip : completeTripList)
			completeTripDistance += tmpTrip.getTripDistance();
			
		if(numberOfTrips>0){
			averageTripDistanceValue = completeTripDistance/numberOfTrips;
			if(writeAsCsv)
				CsvHandler.writeAverageTripDistanceAsCsv(averageTripDistanceValue, this.analysisYear, this.analysisMonth, this.analysisDay);
					
		}
		return averageTripDistanceValue;
	}
	
	
	/**
	 * This function calculates the average trip duration by a set date.
	 * 
	 * It uses JODA-Time to calculate the difference between trips,
	 * creates an absolute summation and divides it through the number of trips.
	 * 
	 * @return Average trip duration, as a double value.
	 */
	public double getAverageTripDurationByDate(){
		LinkedList<Trip> completeTripList = createListOfAllTripsByDate();
			
		int numberOfTrips = completeTripList.size();
		Period completeTripDuration = new Period();
		double averageTripDurationValue = 0;
			
		for(Trip tmpTrip : completeTripList)
			completeTripDuration.plus(tmpTrip.getTripDuration());
			
		if(numberOfTrips>0){
			averageTripDurationValue = completeTripDuration.getMinutes()/numberOfTrips;
			if(writeAsCsv)
				CsvHandler.writeAverageTripDurationAsCsv(averageTripDurationValue, this.analysisYear, this.analysisMonth, this.analysisDay);					
		}
		return averageTripDurationValue;
	}
	
	
	/**
	 * This function creates a tuple list of all cars and their 
	 * respective total number of usages (by date).
	 * 
	 * Calls method: createTupleListOfAllCarsAndItsTripsByDate()
	 * 
	 * Example output:
	 * 	- {("B-GO-1234", 24), ("B-GO-5678", 4)}
	 * 
	 * @return LinkedList<Tuple<String, Integer>>
	 */
	public LinkedList<Tuple<String, Integer>> createTupleListOfAllCarsAndItsNumberOfUsageByDate(){
		LinkedList<Tuple<String, LinkedList<Trip>>> tupleListOfAllCarsAndItsTrips = createTupleListOfAllCarsAndItsTripsByDate();
		LinkedList<Tuple<String, Integer>> listOfAllCarsAndItsNumberOfUsage = new LinkedList<Tuple<String, Integer>>();
		
		for(Tuple<String, LinkedList<Trip>> tupleOfCarAndItsTrips : tupleListOfAllCarsAndItsTrips)
			listOfAllCarsAndItsNumberOfUsage.add(
				new Tuple<String, Integer>(
					tupleOfCarAndItsTrips.getFirstValue(),
					tupleOfCarAndItsTrips.getSecondValue().size()
				)
			);
			
		if(this.writeAsCsv){
			CsvHandler.writeListOfCarsPlatesAndNumberOfMovementsAsCsv(listOfAllCarsAndItsNumberOfUsage, this.analysisYear, this.analysisMonth, this.analysisDay);
		}
			
		return listOfAllCarsAndItsNumberOfUsage;
	}
	
	
	/**
	 * The method 'createTupleListOfAllCarsAndItsTripsByDate()' generates a list of cars and its absolut number of usages by date.
	 * 
	 * Called by: createTupleListOfAllCarsAndItsNumberOfUsageByDate()
	 * 
	 * It first iterates over all available analyzed cars,
	 * and as a second step counts the number of the respective trips.
	 * 
	 * @return LinkedList<Tuple<String, LinkedList<Trip>>>
	 */
	private LinkedList<Tuple<String, LinkedList<Trip>>> createTupleListOfAllCarsAndItsTripsByDate(){
		LinkedList<Trip> completeTripList = createListOfAllTripsByDate();
		LinkedList<Tuple<String, LinkedList<Trip>>> tupleListOfAllCarsAndItsTrips = new LinkedList<Tuple<String, LinkedList<Trip>>>();

		if(null!=completeTripList && !completeTripList.isEmpty()){
			LinkedList<String> carList = DatabaseHandler.getStringListOfAllCarsFromCar2GoDatabase();
			for(String tmpCar : carList){
				LinkedList<Trip> tripListOfTmpCar = new LinkedList<Trip>();
				for(Trip tmpTripFromCompleteList : completeTripList)
					if(tmpTripFromCompleteList.getCar().equalsIgnoreCase(tmpCar))
						tripListOfTmpCar.add(tmpTripFromCompleteList);
				
				Tuple<String, LinkedList<Trip>> carAndItsTripsTuple = new Tuple<String, LinkedList<Trip>>(tmpCar, tripListOfTmpCar);
				tupleListOfAllCarsAndItsTrips.add(carAndItsTripsTuple);
			}
		}
		return tupleListOfAllCarsAndItsTrips;
	}
	
	
	/**
	 * This method creates a list of all trips, absolved on a specific day.
	 * Therefore a trip-model is used (Trip.java).
	 * 
	 * The method calls 'iterateOverStatesByTripList()'-function 
	 * for iterating over all states. 
	 * 
	 * @return LinkedList<Trip> (Trip-List)
	 */
	private LinkedList<Trip> createListOfAllTripsByDate(){
		LinkedList<Trip> tripList = new LinkedList<Trip>();
		LinkedList<Tuple<String, LinkedList<State>>> tupleListOfCarsAndItsStates = createTupleListOfAllCarsAndItsStatesByDate();
		for(Tuple<String, LinkedList<State>> tupleOfCarAndItsStates : tupleListOfCarsAndItsStates){
			String tmpCar = tupleOfCarAndItsStates.getFirstValue();
			LinkedList<State> tmpCarStatesList = tupleOfCarAndItsStates.getSecondValue();
			iterateOverStatesByTripList(tripList, tmpCar, tmpCarStatesList);
		}
		return tripList;
	}

	
	/**
	 * This function iterates over all states by a given car-plate (as string value)
	 * and state list. 
	 * 
	 * All found trips will be added to a given list of trips. 
	 * 
	 * Called by: createListOfAllTripsByDate().
	 * 
	 * @param tripList:				List of trips (empty at the beginning). 
	 * @param tmpCar:				The car to analyze (car-plate as string-value).
	 * @param tmpCarStatesList:		All analyzed states of the analyzed car. 
	 */
	private void iterateOverStatesByTripList(LinkedList<Trip> tripList, String tmpCar, LinkedList<State> tmpCarStatesList) {
		for(int stateIndex = 0; stateIndex < tmpCarStatesList.size(); stateIndex++){
			State firstState = tmpCarStatesList.get(stateIndex);
			State secondStateToCompare = null; State thirdStateIsDriving = null;
				
			if(tmpCarStatesList.size() > stateIndex+1){
				secondStateToCompare = tmpCarStatesList.get(++stateIndex);
				if(tmpCarStatesList.size() > stateIndex+2)
					thirdStateIsDriving = tmpCarStatesList.get(++stateIndex);
			}
			
			if(null!=secondStateToCompare){
				Coordinate startCoordinate = new Coordinate(firstState.getStatesLatitudeCoordinates(), firstState.getStatesLongitudeCoordinates());
					
				if(State.hasPairOfStatesSameCoordinates(firstState, secondStateToCompare))
					break;
					
				Coordinate endCoordinate = null;
				if(null!=thirdStateIsDriving){
					if(State.hasPairOfStatesSameCoordinates(secondStateToCompare, thirdStateIsDriving)){
						endCoordinate = new Coordinate(secondStateToCompare.getStatesLatitudeCoordinates(), secondStateToCompare.getStatesLongitudeCoordinates());
					} else{
						for(int endCoordinateIndex = stateIndex; endCoordinateIndex < tmpCarStatesList.size(); endCoordinateIndex++){
							State potentialEndState = tmpCarStatesList.get(endCoordinateIndex);
							if(State.hasPairOfStatesSameCoordinates(thirdStateIsDriving, potentialEndState))
								endCoordinate = new Coordinate(secondStateToCompare.getStatesLatitudeCoordinates(), secondStateToCompare.getStatesLongitudeCoordinates());
						}
					}
				} else{
					endCoordinate = new Coordinate(secondStateToCompare.getStatesLatitudeCoordinates(), secondStateToCompare.getStatesLongitudeCoordinates());
				}
				
				Timestamp startTime = firstState.getStatesTimestamp();
				Timestamp endTime = secondStateToCompare.getStatesTimestamp();
					
				Trip tmpTrip = new Trip(startCoordinate, endCoordinate, tmpCar, endTime);
				tmpTrip.setTripDuration(startTime, endTime);
				tripList.add(tmpTrip);
			}
		}
	}
	
	
	/**
	 * This method creates a tuple-list of all cars and its analyzed states (by date).
	 * 
	 * Calls method: 'getListOfStatesOfSingleCarByPlateAndDate()'
	 * For receiving state-lists for each car. 
	 * 
	 * Exemplary-Output: 
	 * {("B-GO-1234", {state1,state2,state3}), ("B-GO-5678", {state4,state5,state6})}
	 * 
	 * @return LinkedList<Tuple<String, LinkedList<State>>>
	 */
    public LinkedList<Tuple<String, LinkedList<State>>> createTupleListOfAllCarsAndItsStatesByDate(){
    	LinkedList<Tuple<String, LinkedList<State>>> tupleListOfCarsAndItsStates = new LinkedList<Tuple<String, LinkedList<State>>>();
    	LinkedList<String> listOfExistingCarsFromCar2GoDatabase = DatabaseHandler.getStringListOfAllCarsFromCar2GoDatabase();
    	for(String tmpCarPlateAsString : listOfExistingCarsFromCar2GoDatabase){
    	   	LinkedList<State> stateLinkedList = getListOfStatesOfSingleCarByPlateAndDate(tmpCarPlateAsString);
    	    		
    	   	Tuple<String, LinkedList<State>> tmpCarAndItsStateListTuple = new Tuple<String, LinkedList<State>>(tmpCarPlateAsString, stateLinkedList);
    	   	tupleListOfCarsAndItsStates.add(tmpCarAndItsStateListTuple);
    	}
    		
    	return tupleListOfCarsAndItsStates;
    }
    
    
    /**
     * The function 'getListOfStatesOfSingleCarByPlateAndDate(String carPlate)' takes a single
     * string parameter for iterating over a cars state-list. 
     * 
     * All collected states are returned as a linked list of states.
     * 
     * @param carPlate:		Car-Plate as a string-value (like 'B-GO-1234')
     * 
     * @return LinkedList<State>
     */
    private LinkedList<State> getListOfStatesOfSingleCarByPlateAndDate(String carPlate){
    	LinkedList<State> carSpecificStateList = new LinkedList<State>();
    	String psqlQuery = createPsqlQueryForSelectingAllStatesByDate(); 
										
    	if(null!=carPlate)
    		psqlQuery = createPsqlQueryForSelectingAllStatesByDateAndCarPlate();
			
    	try {
			PreparedStatement selectExistingCarPlatesInDatabase = psqlConnection.prepareStatement(psqlQuery);
			selectExistingCarPlatesInDatabase.setString(1, carPlate);
				
			ResultSet statesResultSet =  selectExistingCarPlatesInDatabase.executeQuery();
			
				
			while(statesResultSet.next()){
				createStateByResultSet(carSpecificStateList, statesResultSet);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return carSpecificStateList;
    }

    
    /**
     * This method creates a specific state object by a given result-set object.
     * 
     * @param carSpecificStateList:		State list where to add the state (empty at the beginning).
     * @param statesResultSet:			ResultSet (from PostgreSQL-Database; Contains information about a car´s-states).
     * @throws SQLException:			Simple SQL-Exception; Will be thrown in case of syntactic SQL-errors. 
     */
	private void createStateByResultSet(LinkedList<State> carSpecificStateList, ResultSet statesResultSet) throws SQLException {
		double statesLatitudeCoordinates 	= statesResultSet.getDouble("coordinates_x");
		double statesLongitudeCoordinates 	= statesResultSet.getDouble("coordinates_y");
		Timestamp statesTimestamp 			= statesResultSet.getTimestamp("time_of_documentation");
		int statesCarFuel 					= statesResultSet.getInt("car_fuel");
		String statesCarInteriorState 		= statesResultSet.getString("car_interior_state");
		String statesCarExteriorState 		= statesResultSet.getString("car_exterior_state");
		int statesParkingSlotsUsed 			= statesResultSet.getInt("parking_slot_used_capacity");
			
		carSpecificStateList.add( 
			new State(	statesLatitudeCoordinates, statesLongitudeCoordinates, statesTimestamp, 
						statesCarFuel, statesCarInteriorState, statesCarExteriorState, statesParkingSlotsUsed)
		);
	}


	/**
	 * This method creates a PostgreSQL-Query for selecting
	 * all states by date and given car-plate. 
	 * 
	 * Query is returned as a string value. 
	 * 
	 * @return String (PsqlQuery)
	 */
	private String createPsqlQueryForSelectingAllStatesByDateAndCarPlate() {
		String psqlQuery;
		psqlQuery = 	"SELECT DISTINCT ON (\"coordinates_x\", \"coordinates_y\") * " + //ON (\"coordinates_x\", \"coordinates_y\")
						"FROM \"state\" "    +
						"WHERE (\"time_of_documentation\"" +
							"BETWEEN 	to_timestamp('" + this.analysisYear + "-" + this.analysisMonth + "-" + this.analysisDay + " 00:00', 'YYYY-MM-DD HH24:MI')" +
							"AND 		to_timestamp('" + this.analysisYear + "-" + this.analysisMonth + "-" + this.analysisDay + " 23:59', 'YYYY-MM-DD HH24:MI'))" +
							"AND 		\"car_plate\" LIKE ?;";
		return psqlQuery;
	}

	
	/**
	 * This method creates a PostgreSQL-Query for selecting
	 * all states by date. 
	 * 
	 * Query is returned as a string value. 
	 * 
	 * @return String (PsqlQuery)
	 */
	private String createPsqlQueryForSelectingAllStatesByDate() {
		String psqlQuery =  "SELECT DISTINCT ON (\"coordinates_x\", \"coordinates_y\") * " + //ON (\"coordinates_x\", \"coordinates_y\")
    					    "FROM \"state\" "    +
						    "WHERE (\"time_of_documentation\"" +
								"BETWEEN 	to_timestamp('" + this.analysisYear + "-" + this.analysisMonth + "-" + this.analysisDay + " 00:00', 'YYYY-MM-DD HH24:MI')" +
								"AND 		to_timestamp('" + this.analysisYear + "-" + this.analysisMonth + "-" + this.analysisDay + " 23:59', 'YYYY-MM-DD HH24:MI'));";
		return psqlQuery;
	}
    
    
	/**
	 * This method returns a tuple list of all cars and their respective number of states
	 * on an analyzed date. 
	 * 
	 * Exemplary-Output: 
	 * {("B-GO-1234",2098), ("B-GO-5678", 2097)} 
	 *
	 * @return LinkedList<Tuple<String, Integer>>
	 */
    public LinkedList<Tuple<String, Integer>> getListOfCarsPlatesAndNumberOfStatesByDate(){    		
   		LinkedList<Tuple<String, LinkedList<State>>> carPlatesAndItsStateList = createTupleListOfAllCarsAndItsStatesByDate();
   		LinkedList<Tuple<String, Integer>> carPlatesAndItsNumberOfStates = new LinkedList<Tuple<String, Integer>>();
    		
   		for(Tuple<String, LinkedList<State>> tmpCarPlateAndItsMovements : carPlatesAndItsStateList){
   			int numberOfMovements = tmpCarPlateAndItsMovements.getSecondValue().size();
   			if(numberOfMovements>0)
   				numberOfMovements--;
    			
   			carPlatesAndItsNumberOfStates.add(
   				new Tuple<String, Integer>(
   					tmpCarPlateAndItsMovements.getFirstValue(),
   					numberOfMovements
   				)
   			);
    	}
    	return carPlatesAndItsNumberOfStates;	
    }
}