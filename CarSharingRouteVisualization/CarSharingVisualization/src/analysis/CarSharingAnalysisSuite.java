package analysis;

import java.sql.Connection;
import java.util.Scanner;

import database_handler.DatabaseHandler;

/**
 * This class represents an analysis suite
 * and calls functionalities from CarSharingAnalysisMethods.java. 
 * 
 * Via startUserConsoleInteraction() a user-console-interaction could be 
 * started and therefore analysis-methods triggered.
 * 
 * @author niklasrappel
 */
public class CarSharingAnalysisSuite {

	public static int UPDATE_CAR2GO_DATABASE_ITERATIONS_TEST = 3;
	public static int UPDATE_CAR2GO_DATABASE_ITERATIONS_PER_DAY = 8640000;
	
	private static final String WELCOME_STRING = 	" #####     #    ######         #####         #####        \n" +  
													"#     #   # #   #     #       #     #       #     #  #### \n" +  
													"#        #   #  #     #             #       #       #    #\n" + 
													"#       #     # ######  #####  #####  ##### #  #### #    #\n" + 
													"#       ####### #   #         #             #     # #    #\n" + 
													"#     # #     # #    #        #             #     # #    #\n" +
													" #####  #     # #     #       #######        #####   #### \n" + 
												  	"\nWelcome to the car2go-database-connectivity!\n";
	private static final String SELECTION_STRING = 	"Please select one of the following options: \n" +
												  	"\t(1) Update car2go-database.\n" +
												  	"\t(2) Store car2go car-states from database as .csv-file.\n" +
												  	"\t(3) Store cars and it´s number of movements by date as .csv-file.\n" +
												  	"\t(4) Do time-analysis and store results as .csv-file.\n" + 
												  	"\t(5) Calculate average trip-distance and save results as .csv-file.\n" + 
												  	"\t(6) Calculate average trip-duration and save results as .csv-file.\n" + 
										
												  	"\t(7) Exit...";
	private static final String OPTION_NUMBER_OF_UPDATE_ITERATIONS = "Choose a number of update-iterations: \n" +
								"\t(a) " + UPDATE_CAR2GO_DATABASE_ITERATIONS_TEST + "\n" + 
								"\t(b) " + UPDATE_CAR2GO_DATABASE_ITERATIONS_PER_DAY;
	private static final String ENTER_YEAR = "Please enter the date of interest for analyze:\n" +
											 "\tEnter the 'year' as a numeric value, please:";
	private static final String ENTER_MONTH = "\tEnter the 'month' as a numeric value (integer, 1:=january, 2:=february, ...), please:";
	private static final String ENTER_DAY 	= "\tEnter the 'day' as a numeric value, please:";
	
	
	
	
	/**
	 * MAIN_METHOD
	 * without_parameter
	 * 
	 */
	public static void main(String[] args){
		/**
		 * -- AUTO_START -------------------
		 * -- START USER/CONSOLE INTERACTION:
		 */
		startUserConsoleInteraction();
	}
	
	
	/**
	 * This method starts the user-console-interaction using 
	 * scanner functionality to read user inputs. 
	 * 
	 * USER-OPTIONS:
	 * 	- Analysis-methods 
	 * 			(1) Update car2go-database
	 *			(2) Store car2go car-states from database as CSV-File
	 *			(3) Store cars and it´s number of movements by date as CSV-File
	 *			(4) Do time-analysis and store results as CSV-File
	 *			(5) Calculate average trip-distance and save results as CSV-File
	 *			(6) Calculate average trip-duration and save results as CSV-File 
	 *  - Set analysis-date 
	 *  - Write as CSV-File (yes/no)
	 *  - Exit user-console-interaction
	 *  
	 *  Database:
	 *  First connection to the PostgreSQL-Database will be established.
	 *  
	 *  Analysis-Methods:
	 *  Object of type 'CarSharingAnalysisMethods.java' will be created.
	 *  
	 */
	private static void startUserConsoleInteraction(){
		System.out.println(WELCOME_STRING);
		System.out.println(SELECTION_STRING);
		
		Connection psqlConnection = DatabaseHandler.establishDatabaseConnection();
		
		Scanner userInputScanner = new Scanner(System.in);
		int crntUserInput = userInputScanner.nextInt();
		
		System.out.println(ENTER_YEAR);
		int userInputForYear = userInputScanner.nextInt();
		System.out.println(ENTER_MONTH);
		int userInputForMonth = userInputScanner.nextInt();
		System.out.println(ENTER_DAY);
		int userInputForDay = userInputScanner.nextInt();
		
		CarSharingAnalysisMethods analysisFactory = new CarSharingAnalysisMethods(
				psqlConnection,
				userInputForYear,
				userInputForMonth,
				userInputForDay,
				true
		);

		startAnalysisOptionModeByUserInput(userInputScanner, crntUserInput, analysisFactory);
	}


	/**
	 * This method starts the analysis-option-mode.
	 * 
	 * A user could choose between the following options:
	 * 			(1) Update car2go-database
	 *			(2) Store car2go car-states from database as CSV-File
	 *			(3) Store cars and it´s number of movements by date as CSV-File
	 *			(4) Do time-analysis and store results as CSV-File
	 *			(5) Calculate average trip-distance and save results as CSV-File
	 *			(6) Calculate average trip-duration and save results as CSV-File 
	 *
	 * @param userInputScanner:		Scanner for reading further user input.
	 * @param crntUserInput:		The current input of a user: Represents the analysis-option in this case.
	 * @param analysisFactory:		Analysis-Factory that contains all analysis-methods, that will be triggered depending on users choice. 
	 */
	private static void startAnalysisOptionModeByUserInput(Scanner userInputScanner, int crntUserInput, CarSharingAnalysisMethods analysisFactory) {
		switch(crntUserInput){
		case 1:
			System.out.println(OPTION_NUMBER_OF_UPDATE_ITERATIONS);
			String selectedOptionOfUpdateIterations = userInputScanner.next();
			if(selectedOptionOfUpdateIterations.equals("a") || selectedOptionOfUpdateIterations.equals("A")){
				DatabaseHandler.doPeriodicUpdateOfCar2GoDatabase(UPDATE_CAR2GO_DATABASE_ITERATIONS_TEST);
			}
			else if(selectedOptionOfUpdateIterations.equals("b") || selectedOptionOfUpdateIterations.equals("B")){
				DatabaseHandler.doPeriodicUpdateOfCar2GoDatabase(UPDATE_CAR2GO_DATABASE_ITERATIONS_PER_DAY);
			}
			else{
				System.out.println("ERROR: Incorrect user input! Valid inputs are 'a' or 'b' (without ')!");
			}
			break;
		case 2:
			System.out.println("INFO: Started Csv-Export ...");
			CsvHandler.writeStatesFromAllCarsAndItsStatesAsCsv(analysisFactory);
			System.out.println("INFO: Csv-Export finished!");
			break;
		case 3:
			System.out.println("INFO: Started Csv-Export ...");
			analysisFactory.createTupleListOfAllCarsAndItsNumberOfUsageByDate();		
			System.out.println("INFO: Csv-Export finished!");
			break;
		case 4:
			System.out.println("INFO: Started Csv-Export ...");
			analysisFactory.getCarsAndItsTripsByDate();
			System.out.println("INFO: Csv-Export finished!");
			break;
		case 5:
			System.out.println("INFO: Started Csv-Export ...");
			analysisFactory.getAverageTripDistanceByDate();
			System.out.println("INFO: Csv-Export finished!");
			break;
		case 6:
			System.out.println("INFO: Started Csv-Export ...");
			analysisFactory.getAverageTripDurationByDate();
			System.out.println("INFO: Csv-Export finished!");
			break;
		case 7:
			System.out.println(	"INFO: Thanks for using the car2go-analysis-suite!\n" +
					"Goodbye...");
			userInputScanner.close();
			return;
		default:
			System.out.println("ERROR: Incorrect user input! Valid inputs are '1', '2' or '3' (without ')!");
			userInputScanner.close();
		}
	}
}
