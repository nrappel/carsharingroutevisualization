package database_handler;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import analysis.CsvHandler;
import web_crawler.Car2GoWebCrawler;
import web_crawler_model.Coordinate;
import web_crawler_model.ParkingSpot;
import web_crawler_model.Position;
import web_crawler_model.Vehicle;

/**
 * DatabaseHandler.java contains methods for interacting with the PostgreSQL-Database. 
 * Therefore a JDBC-Driver for PostgreSQL is used for establishing connections to the database.
 * 
 * The class contains methods for connection establishment and data-manipulation. 
 * SQL-Statements are all wrapped as prepared statements (security-issues).
 * 
 * 
 * @author niklasrappel
 */
public class DatabaseHandler {

	
	/**
	 * DATABASE_SETTINGS
	 */
	private static final String JDBC_DRIVER = "org.postgresql.Driver";
	private static final String DATABASE_PASSWORD = "";
	private static final String DATABASE_USER = "niklasrappel";
	private static final String DATABASE_NAME = "carsharingdb";
	private static final String DATABASE_PORT = "5432";
	private static final String DATABASE_HOST = "localhost";
	private static final String DATABASE_DRIVER = "jdbc:postgresql";
	
	/**
	 * CONNECTION (PostgreSQL)
	 */
	private static Connection psqlConnection;
	
	/**
	 * PSQL-QUERIES
	 */
	private static final String PSQL_QUERY_SELECT_CARPLATE_FROM_CAR = "SELECT \"car_plate\" FROM car;";
	private static final String PSQL_QUERY_INSERT_CAR = "INSERT INTO car (car_plate, car_vin, car_engine_type) VALUES (?, ?, ?);";
	private static final String PSQL_QUERY_SELECT_CAR_WHERE_PLATE = "SELECT * FROM car WHERE car_plate=?;";
	private static final String PSQL_QUERY_INSERT_ADDRESS = "INSERT INTO address (coordinates_x, coordinates_y, address_number, address_street, address_plz, address_city, address_is_parking_slot) VALUES (?, ?, ?, ?, ?, ?, ?);";
	private static final String PSQL_QUERY_INSERT_PARKING_SLOT = "INSERT INTO parking_slot (parking_slot_name, parking_slot_total_capacity, parking_slot_has_charging_pole) VALUES (?, ?, ?);";
	private static final String PSQL_QUERY_SELECT_ADDRESS_WHERE_COORDINATES = "SELECT * FROM address WHERE coordinates_x=? AND coordinates_y=?;";
	private static final String PSQL_QUERY_INSERT_STATE = "INSERT INTO state (coordinates_x, coordinates_y, car_plate, time_of_documentation, car_fuel, car_interior_state, car_exterior_state, parking_slot_used_capacity) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
	
	/**
	 * UTILITIES
	 */
	private boolean stopOnError;
    private boolean autoCommit;
    private PrintWriter logWriter = new PrintWriter(System.out);
    private PrintWriter errorLogWriter = new PrintWriter(System.err);
    private boolean fullLineDelimiter = false;
    private String delimiter = DEFAULT_DELIMITER;
    private static final String DEFAULT_DELIMITER = ";";
    private static final String CAR2GO_WEB_KEY = "car2gowebsite";
    private static final String CAR2GO_LOCATION = "Berlin";
    private final static String DB_INFO_CONNECTION_ESTABLISHED = "DB_INFO: Connection to database was established!";
	private final static String DB_ERROR_CONNECTION_NOT_ESTABLISHED = "DB_ERROR: Connection to database could not be established!";
    
    /**
     * PATHES
     */
    public static final String PATH_TO_SQLQUERY_INIT_DATABASE = "/Users/niklasrappel/Dropbox/UNI_WUE/SOSE_2015/git_repository/CarSharingRouteVisualization/CarSharingRouteVisualization/er-model/er_diagram.sql";
	public static final String PATH_TO_CSV_OUTPUT = "/Users/niklasrappel/Dropbox/UNI_WUE/SOSE_2015/git_repository/CarSharingRouteVisualization/CarSharingVisualization/csv_analysis";
	
	
	
	
	/**
	 * This method establishes a connection to the PostgreSQL-Database.
	 * 
	 * Via the static method getConnection() of DriverManager.class you have to set 
	 * following parameters (exemplary values):
	 * 		- Database-Driver: 	 jdbc:postgresql
	 * 		- Host: 		  	 localhost
	 * 		- Port:			  	 5432
	 *  	- Database-Name:	 carsharingdb
	 *  	- User-Name:		 niklasrappel
	 *  	- DB-Password:		 ""
	 */
	public static Connection establishDatabaseConnection(){
		try {
			if(null!=psqlConnection)
				psqlConnection.close();
			
			Class.forName(JDBC_DRIVER);
			psqlConnection = DriverManager.getConnection(
					DATABASE_DRIVER + "://" + 
					DATABASE_HOST + ":" + 
					DATABASE_PORT + "/" + 
					DATABASE_NAME
					,
					DATABASE_USER, DATABASE_PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		if (psqlConnection != null) 
			System.out.println(DB_INFO_CONNECTION_ESTABLISHED);
		else 
			System.out.println(DB_ERROR_CONNECTION_NOT_ESTABLISHED);
		
		return psqlConnection;
	}
	
	
	/**
	 * This method closes an existing database-connection.
	 * Therefore first it has to proof, if there´s an existing database-connection available.
	 * 
	 */
	public static void closeDatabaseConnection(){
		try {
			if(null!=psqlConnection)
				psqlConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * DATABASE_UTILITIES:
	 * 
	 */
	private String getDelimiter() {
        return delimiter;
    }

    private void print(Object o) {
        if (logWriter != null) {
            System.out.print(o);
        }
    }

    private void println(Object o) {
        if (logWriter != null) {
            logWriter.println(o);
        }
    }
 
    private void printlnError(Object o) {
        if (errorLogWriter != null) {
            errorLogWriter.println(o);
        }
    }

    private void flush() {
        if (logWriter != null) {
            logWriter.flush();
        }
        if (errorLogWriter != null) {
            errorLogWriter.flush();
        }
    }
	
    
    
    /**
     * This function simulates a period database-update by a given 
     * integer heart-beat (total number of runs). 
     * 
     * Method is used for long-time-analysis (specific time-ranges).
     * 
     * @param totalNumberOfRuns:		Number of update iterations.
     */
    public static void doPeriodicUpdateOfCar2GoDatabase(int totalNumberOfRuns){
    	if(0 < totalNumberOfRuns){
    		try{
        		while(0 < totalNumberOfRuns){
        			updateCar2GoDatabase();
        			totalNumberOfRuns--;
            		Thread.sleep(60000);
        		}
    			
        	}catch(InterruptedException ex){
        		ex.printStackTrace();
        	}
    		
    	}
    }

    
    /**
     * The method 'updateCar2GoDatabase()' updates the car2go-database.
     * 
     * Therefore it does a update in the following structure:
     * 	- Update cars
     * 	- Update addresses
     *  - Update parking slots
     *  - Update states (uses information from the entities above) 
     *  
     *  Calls 'updateStateTableForCar2GoDatabase()':
     *  - Using vehicle-list
     *  - Using pariking-spot-list
     *  
     *  For successful update the car2go-web-key may has to be
     *  modified (CAR2GO_WEB_KEY; final static String).
     */
    private static void updateCar2GoDatabase(){
    	Car2GoWebCrawler crawler = new Car2GoWebCrawler(CAR2GO_WEB_KEY);

    	List<Vehicle> listOfAvailableCars = crawler.getAllFreeVehicles(CAR2GO_LOCATION, CAR2GO_WEB_KEY);
    	List<ParkingSpot> listOfParkingSpots = crawler.getAllParkingSpots(CAR2GO_LOCATION, CAR2GO_WEB_KEY);
    	
    	if(null!=psqlConnection){
    		SimpleDateFormat currentTimeFormat = new SimpleDateFormat("MMddyyyy_HH:mm");

    		System.out.println("Started updateing car2go database at time: " + currentTimeFormat.format(System.currentTimeMillis()));
    		updateStateTableForCar2GoDatabase(listOfAvailableCars,listOfParkingSpots);
    		CsvHandler.writeDataFromPostgreSQLDatabaseAsCsv(psqlConnection, "state", null, PATH_TO_CSV_OUTPUT);    		
    		System.out.println("Updateing car2go database was successfull!");
    	}
    }

    
    /**
     * This method updates the state table by a given lists of vehicles an parking spots.
     * 
     * Method is called by updateCar2GoDatabase() and follows same structure.
     * 	- Update cars
     * 	- Update addresses
     *  - Update parking slots
     *  - Update states (uses information from the entities above) 
     * 
     * @param listOfAvailableCars:		List of analyzed cars.
     * @param listOfParkingSpots:		List of parking spots.
     */
	private static void updateStateTableForCar2GoDatabase(List<Vehicle> listOfAvailableCars,List<ParkingSpot> listOfParkingSpots)  {
		for(Vehicle tmpCar : listOfAvailableCars){
			Position pos = tmpCar.getPosition();
			Coordinate c = pos.getCoordinate();
			
			double tmpCarLatitude = c.getLatitude();
			double tmpCarLongitude = c.getLongitude();
			
			updateCarTableForCar2GoDatabase(tmpCar);
			int tmpParkingSpotUsedCapicity = updateAddressTableForCar2GoDatabase(listOfParkingSpots,pos, tmpCarLatitude, tmpCarLongitude);
			updateStateTableForCar2GoDatabase(tmpCar, tmpCarLatitude, tmpCarLongitude, tmpParkingSpotUsedCapicity);	
		}	
	}

	
	/**
	 * This function updates the 'state'-table for a single record. 
	 * Method is called by 'updateStateTableForCar2GoDatabase()'.
	 * 
	 * @param tmpCar:							Car-Model-Object
	 * @param tmpCarLatitude:					Latitude-Value
	 * @param tmpCarLongitude:					Longitude-Value
	 * @param tmpParkingSpotUsedCapicity:		Used parking-spots
	 */
	private static void updateStateTableForCar2GoDatabase(Vehicle tmpCar, double tmpCarLatitude, double tmpCarLongitude, int tmpParkingSpotUsedCapicity){
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		try {
			PreparedStatement insertStatePreparedStatement = psqlConnection.prepareStatement(PSQL_QUERY_INSERT_STATE);
			
			insertStatePreparedStatement.setDouble(1, tmpCarLatitude);
			insertStatePreparedStatement.setDouble(2, tmpCarLongitude);
			insertStatePreparedStatement.setString(3, tmpCar.getPlate());
			insertStatePreparedStatement.setTimestamp(4, timestamp);
			insertStatePreparedStatement.setInt(5, Integer.parseInt(tmpCar.getFuel()));
			insertStatePreparedStatement.setString(6, tmpCar.getInterior());
			insertStatePreparedStatement.setString(7, tmpCar.getExterior());
			insertStatePreparedStatement.setInt(8, tmpParkingSpotUsedCapicity);
			insertStatePreparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	
	/**
	 * This method updates the address-table of the car2go-database.
	 * Therefore it extracts specific addresses from a position-object.
	 * 
	 * Method is called by 'updateStateTableForCar2GoDatabase()'.
	 * Address is parsed via method 'parseCar2GoAddress()' out of Car2GoWebCrawler.java.
	 * 
	 * @param listOfParkingSpots:		List of existing parking-spots.
	 * @param pos:						Position of the vehicle (Position.java / model).
	 * @param tmpCarLatitude:			Latitude-Value.
	 * @param tmpCarLongitude:			Longitude-Value.
	 *
	 * @return: Absolute number of used parking spots (as Integer).
	 */
	private static int updateAddressTableForCar2GoDatabase(List<ParkingSpot> listOfParkingSpots, Position pos, double tmpCarLatitude, double tmpCarLongitude){
		String car2GoAddress = pos.getAdress();
		HashMap<String, String> car2goAddressHashmap = Car2GoWebCrawler.parseCar2GoAddress(car2GoAddress);
		
		int tmpParkingSlotUsedCapicity = 0;
		try {
			PreparedStatement selectExistingAddressesInDatabase = psqlConnection.prepareStatement(PSQL_QUERY_SELECT_ADDRESS_WHERE_COORDINATES);
			selectExistingAddressesInDatabase.setDouble(1, tmpCarLatitude);
			selectExistingAddressesInDatabase.setDouble(2, tmpCarLongitude);
			ResultSet car2goAddressRS = selectExistingAddressesInDatabase.executeQuery();
			
			if(!car2goAddressRS.next()){
				PreparedStatement insertAddressPreparedStatement = initInsertddressPreparedStatement(tmpCarLatitude, tmpCarLongitude, car2goAddressHashmap);
				boolean addressIsParkingSlot = false;
					
				for(ParkingSpot tmpSlotInCity : listOfParkingSpots){
					Coordinate tmpParkingSpotCoordinate = tmpSlotInCity.getCoordinate();
					
					double tmpParkingSpotLatitude = tmpParkingSpotCoordinate.getLatitude();
					double tmpParkingSpotLongitude = tmpParkingSpotCoordinate.getLongitude();
				
					if((tmpParkingSpotLatitude==tmpCarLatitude) && (tmpParkingSpotLongitude==tmpCarLongitude)){
						addressIsParkingSlot = true;

						PreparedStatement insertParkingSlotPreparedStatement = psqlConnection.prepareStatement(PSQL_QUERY_INSERT_PARKING_SLOT);
						insertParkingSlotPreparedStatement.setString(1, tmpSlotInCity.getName());
						insertParkingSlotPreparedStatement.setInt(2, tmpSlotInCity.getTotalCapacity());
						insertParkingSlotPreparedStatement.setBoolean(3, tmpSlotInCity.isChargingPole());					
						insertParkingSlotPreparedStatement.executeUpdate();
						
						tmpParkingSlotUsedCapicity = tmpSlotInCity.getUsedCapacity();
					}
				}
				
				insertAddressPreparedStatement.setBoolean(7, addressIsParkingSlot);
				insertAddressPreparedStatement.executeUpdate();	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return tmpParkingSlotUsedCapicity;
	}

	
	/**
	 * This method inserts a single address-record into the 
	 * respective 'address'-relation (car2go-database).
	 * 
	 * Therefore it:
	 * 	- uses latitude and longitude values given as parameters.
	 *  - takes a hash map of existing addresses.  
	 * 
	 * @param tmpCarLatitude:			Latitude-Value
	 * @param tmpCarLongitude:			Longitude-Value
	 * @param car2goAddressHashmap:		Address-Hash-Map
	 * 
	 * @return: PreparedStatement for inserting address into PostgreSQL-Database. 
	 */
	private static PreparedStatement initInsertddressPreparedStatement(double tmpCarLatitude, double tmpCarLongitude, HashMap<String, String> car2goAddressHashmap){
		try {
			PreparedStatement insertAddressPreparedStatement = psqlConnection.prepareStatement(PSQL_QUERY_INSERT_ADDRESS);
			
			insertAddressPreparedStatement.setDouble(1, tmpCarLatitude);
			insertAddressPreparedStatement.setDouble(2, tmpCarLongitude);
			insertAddressPreparedStatement.setInt(3, Integer.parseInt(car2goAddressHashmap.get("streetNumber")));
			insertAddressPreparedStatement.setString(4, car2goAddressHashmap.get("streetName"));
			insertAddressPreparedStatement.setInt(5, Integer.parseInt(car2goAddressHashmap.get("plz")));
			insertAddressPreparedStatement.setString(6, car2goAddressHashmap.get("city"));
			
			return insertAddressPreparedStatement;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	/**
	 * This method updates the 'car'-table of the car2go-database.
	 * It only inserts new car-records, if there is not a duplicate entry. 
	 * 
	 * @param tmpCar:		The vehicle, that will be inserted (Vehicle.java-Object).
	 * @return
	 */
	private static String updateCarTableForCar2GoDatabase(Vehicle tmpCar){
		try {
			PreparedStatement selectExistingCarPlatesInDatabase = psqlConnection.prepareStatement(PSQL_QUERY_SELECT_CAR_WHERE_PLATE);
			selectExistingCarPlatesInDatabase.setString(1, tmpCar.getPlate());
			ResultSet rs = selectExistingCarPlatesInDatabase.executeQuery();
			String tmpCarPlate = tmpCar.getPlate();

			if(!rs.next()){
				PreparedStatement insertCarPreparedStatement = psqlConnection.prepareStatement(PSQL_QUERY_INSERT_CAR);

				insertCarPreparedStatement.setString(1, tmpCarPlate);
				insertCarPreparedStatement.setString(2, tmpCar.getVin());
				insertCarPreparedStatement.setString(3, tmpCar.getEngineType() );
				insertCarPreparedStatement.executeUpdate();
			}
			return tmpCarPlate;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * This method returns a list of all cars (by its car-plates / string-values). 
	 * Therefore it queries the database via a simple select-query. 
	 * 
	 * @return LinkedList<String>
	 */
	public static LinkedList<String> getStringListOfAllCarsFromCar2GoDatabase(){
    	String psqlCarSelectionQuery = PSQL_QUERY_SELECT_CARPLATE_FROM_CAR;
    	try {
			PreparedStatement psqlCarSelectionPreparedStatement = psqlConnection.prepareStatement(psqlCarSelectionQuery);
			ResultSet carsResultSet = psqlCarSelectionPreparedStatement.executeQuery();
			LinkedList<String> carsFromCar2GoDatabase = new LinkedList<String>();
			while(carsResultSet.next())
				carsFromCar2GoDatabase.add(carsResultSet.getString("car_plate"));
			
			return carsFromCar2GoDatabase;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
    }
	
	
	/**
     * Runs an SQL script (Using Java-Reader)
     *
     * @param reader:			Reader.java-Object for file reading.
     */
    public void runSQLScriptByReader(Reader reader) throws IOException, SQLException {
        try {
            boolean originalAutoCommit = psqlConnection.getAutoCommit();
            try {
                if (originalAutoCommit != this.autoCommit) 
                    psqlConnection.setAutoCommit(this.autoCommit);
  
                runScriptUsingConnectionAndReader(psqlConnection, reader);
            } finally {
                psqlConnection.setAutoCommit(originalAutoCommit);
            }
        } catch (IOException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Error running script.  Cause: " + e, e);
        }
    }

    
    /**
     * Runs an SQL script (read in using the Reader parameter) using the
     * connection passed in.
     *
     * @param conn: 			The connection to use for the script
     * @param reader: 			The source of the script
     * @throws SQLException: 	If any SQL errors occur
     * @throws IOException:		If there is an error reading from the Reader
     */
    private void runScriptUsingConnectionAndReader(Connection conn, Reader reader) throws IOException, SQLException {
        StringBuffer tmpCommand = null;
        try {
            LineNumberReader tmpLineReader = new LineNumberReader(reader);
            String tmpLine = null;
            while ((tmpLine = tmpLineReader.readLine()) != null) {
                if (tmpCommand == null) {
                    tmpCommand = new StringBuffer();
                }
                String trimmedLine = tmpLine.trim();
                if (trimmedLine.startsWith("--")) {
                    println(trimmedLine);
                } else if (trimmedLine.length() < 1
                        || trimmedLine.startsWith("//")) {
                } else if (trimmedLine.length() < 1
                        || trimmedLine.startsWith("--")) {
                } else if (!fullLineDelimiter
                        && trimmedLine.endsWith(getDelimiter())
                        || fullLineDelimiter
                        && trimmedLine.equals(getDelimiter())) {
                    tmpCommand.append(tmpLine.substring(0, tmpLine
                            .lastIndexOf(getDelimiter())));
                    tmpCommand.append(" ");
                    Statement statement = conn.createStatement();

                    println(tmpCommand);

                    boolean hasResults = false;
                    if (stopOnError) {
                        hasResults = statement.execute(tmpCommand.toString());
                    } else {
                        try {
                            statement.execute(tmpCommand.toString());
                        } catch (SQLException e) {
                            e.fillInStackTrace();
                            printlnError("Error executing: " + tmpCommand);
                            printlnError(e);
                        }
                    }

                    if (autoCommit && !conn.getAutoCommit()) {
                        conn.commit();
                    }

                    ResultSet rs = statement.getResultSet();
                    if (hasResults && rs != null) {
                        ResultSetMetaData md = rs.getMetaData();
                        int cols = md.getColumnCount();
                        for (int i = 0; i < cols; i++) {
                            String name = md.getColumnLabel(i);
                            print(name + "\t");
                        }
                        println(DATABASE_PASSWORD);
                        while (rs.next()) {
                            for (int i = 0; i < cols; i++) {
                                String value = rs.getString(i);
                                print(value + "\t");
                            }
                            println(DATABASE_PASSWORD);
                        }
                    }

                    tmpCommand = null;
                    try {
                        statement.close();
                    } catch (Exception e) {}
                    Thread.yield();
                } else {
                    tmpCommand.append(tmpLine);
                    tmpCommand.append(" ");
                }
            }
            if (!autoCommit) {
                conn.commit();
            }
        } catch (SQLException e) {
            e.fillInStackTrace();
            printlnError("Error executing: " + tmpCommand);
            printlnError(e);
            throw e;
        } catch (IOException e) {
            e.fillInStackTrace();
            printlnError("Error executing: " + tmpCommand);
            printlnError(e);
            throw e;
        } finally {
            conn.rollback();
            flush();
        }
    }
}