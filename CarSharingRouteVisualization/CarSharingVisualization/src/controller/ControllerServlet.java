package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import analysis.CarSharingAnalysisMethods;

/**
 * ControllerServler.java delegates all TOMCAT-server-functionality of the project.
 * It redirects JavaServerPages (JSPs) or shows error messages via JavaScript.
 * 
 * This class also allows to define new HTML-Pages (Analysis-Pages).
 * (ATTENTION: You also could define them via Web.xml)
 * 
 * We have to @Override GET and POST-Stub! 
 * 
 * 
 * @author niklasrappel
 */
@WebServlet(name="ControllerServlet",
loadOnStartup = 1,
urlPatterns = {
				"index",
             	"CarTrackerByDate"
				})
public class ControllerServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private String userPath;

	
    /**
     * @see HttpServlet#HttpServlet()
     * 
     * DEFAULT_CONSTRUCTOR:
     * Initialize ControllerServlet procedure.
     */
    public ControllerServlet() {
        super();
        
        try {
			init();
		} catch (ServletException e) {
			e.printStackTrace();
		}    
    }
    
 
	/**
	 * GET_STUB
	 * 
	 * Handles all information sent via GET-Request.
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	userPath = request.getServletPath();
		String url = "/WEB-INF/jsp" + userPath + ".jsp";
		try {
	    	request.getRequestDispatcher(url).forward(request, response);
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	    }	
    }
    

	/**
	 * POST_STUB
	 * 
	 * Handles all information sent via POST-Request.
	 *
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("RequestURI: " + request.getRequestURI());		
		System.out.println("UserPath: "   + userPath);	
		
		if(userPath.equals("/index") || null==userPath) {
			if(null!=request.getParameter("submitTrackCarAnalysisButtonName"))
				createTrackCarByDateServerPage(request, response);
			
			if(null!=request.getParameter("submitCarSharingTrafficAnalysisButtonName"))
				createCarPopulationInDistrictServerPage(request, response);
			
			return;
		}
		
		if(userPath.equals("/CarTrackerByDate")) {
			CarSharingAnalysisMethods.getCarPopulationOfCityDistricts(request);
		}
		
		response.sendRedirect("/CarSharingVisualization/index");
   	}

	
	/**
	 * This method creates a new ServerPage. 
	 * 
	 * @param request:	Respective HttpServletRequest.
	 * @param response:	Respective HttpServletResponse.
	 */
	private void createCarPopulationInDistrictServerPage(HttpServletRequest request, HttpServletResponse response) {
		if(null!=request && null!=response){
			try {
				response.sendRedirect("/CarSharingVisualization/CarPopulationInDistricts");
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	
	/**
	 * This method creates a new ServerPage.
	 *  
	 * Page-Content depends on user input: 
	 * User selects a car-plate of an car that should be analyzed
	 * and data is send to the respective analysis-page. 
	 * 
	 * @param request:	Respective HttpServletRequest.
	 * @param response:	Respective HttpServletResponse.
	 */
	private void createTrackCarByDateServerPage(HttpServletRequest request, HttpServletResponse response){
		if(null!=request && null!=response){
			String carPlate = request.getParameter("carSelector");
			System.out.println("User-Selected car-plate: " + carPlate);
			
			try {
				response.sendRedirect("/CarSharingVisualization/CarTrackerByDate?carPlate="+carPlate);
				
				request.setAttribute("carPlate", carPlate);
				request.getRequestDispatcher("/CarTrackerByDate.jsp").forward(request, response);
			
			} catch (IOException | ServletException e) {
				e.printStackTrace();
			}
		}
	}
}