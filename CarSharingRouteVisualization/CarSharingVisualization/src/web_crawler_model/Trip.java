package web_crawler_model;

import java.sql.Timestamp;

import org.joda.time.DateTime;
import org.joda.time.Period;

/**
 * This class simulates a chain of simulates, without using a list of state-objects. 
 * The class just uses start and a end coordinates.
 * 
 * 
 * @author niklasrappel
 */
public class Trip {

	private String carPlate;
	private Coordinate startCoordinate;
	private Coordinate endCoordinate;
	private double tripDistance;
	private Period tripDuration;
	private Timestamp circaTime;

	
	/**
	 * INITIAL-CONSTRUCTOR
	 * 
	 * @param startCoordinate
	 * @param endCoordinate
	 * @param car
	 * @param circaTime
	 */
	public Trip(Coordinate startCoordinate, Coordinate endCoordinate, String carPlate, Timestamp circaTime) {
		super();
		this.startCoordinate = startCoordinate;
		this.endCoordinate = endCoordinate;
		this.circaTime = circaTime;
		this.tripDistance = Distance.getDistKilometer(startCoordinate, endCoordinate);
		this.setTripDuration(new Period());
		this.carPlate = carPlate;
	}


	/**
	 * GETTER & SETTER
	 * 
	 */
	
	public Coordinate getStartCoordinate() {
		return startCoordinate;
	}


	public Coordinate getEndCoordinate() {
		return endCoordinate;
	}


	public void setStartCoordinate(Coordinate startCoordinate) {
		this.startCoordinate = startCoordinate;
	}


	public void setEndCoordinate(Coordinate endCoordinate) {
		this.endCoordinate = endCoordinate;
	}


	public String getCar() {
		return carPlate;
	}


	public void setCar(String carPlate) {
		this.carPlate = carPlate;
	}


	public Timestamp getCircaTime() {
		return circaTime;
	}


	public void setCircaTime(Timestamp circaTime) {
		this.circaTime = circaTime;
	}

	
	public Period getTripDuration() {
		return tripDuration;
	}

	
	public void setTripDuration(Period tripDuration) {
		this.tripDuration = tripDuration;
	}


	public String getCarPlate() {
		return carPlate;
	}


	public double getTripDistance() {
		return tripDistance;
	}


	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}

	
	public void setTripDistance(double tripDistance) {
		this.tripDistance = tripDistance;
	}


	public void setTripDuration(Timestamp startTime, Timestamp endTime) {
		DateTime startDateTime = new DateTime(startTime);
		DateTime endDateTime = new DateTime(endTime);
		
		this.tripDuration = new Period(startDateTime, endDateTime);
		
	}	
}