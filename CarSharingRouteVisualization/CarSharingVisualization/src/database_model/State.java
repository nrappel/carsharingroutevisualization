package database_model;

import java.sql.Timestamp;

/**
 * This class is the counterpart of the respective 'car'-relation
 * from the Car2Go-Database. 
 * 
 * Model-Class: State.java
 * 
 * 
 * @author niklasrappel
 */
public class State {

	
	private double statesLatitudeCoordinates;
	private double statesLongitudeCoordinates;
	private Timestamp statesTimestamp;
	private int statesCarFuel;
	private String statesCarInteriorState;
	private String statesCarExteriorState;
	private int statesParkingSlotsUsed;
	
	
	/**
	 * Default-Constructor
	 */
	public State(){}

	
	/**
	 * Initial-Constructor
	 * 
	 * @param statesLatitudeCoordinates:		Latitude value (double)
	 * @param statesLongitudeCoordinates:		Longitude value (double)
	 * @param statesTimestamp:					Time of documentation (timestamp)
	 * @param statesCarFuel:					Cars percentage fuel (int)
	 * @param statesCarInteriorState:			Interior state of a car, e.g. the seats (String)
	 * @param statesCarExteriorState:			Exterior state of a car, e.g. the vehicle body (String) 
	 * @param statesParkingSlotsUsed:			Number of used parking slots, if states coordinates match (int)
	 */
	public State(	double statesLatitudeCoordinates, double statesLongitudeCoordinates, 
					Timestamp statesTimestamp, int statesCarFuel,
					String statesCarInteriorState, String statesCarExteriorState, int statesParkingSlotsUsed){
		
		this.statesCarFuel = statesCarFuel;
		this.statesLatitudeCoordinates = statesLatitudeCoordinates;
		this.statesLongitudeCoordinates = statesLongitudeCoordinates;
		this.statesParkingSlotsUsed = statesParkingSlotsUsed;
		this.statesTimestamp = statesTimestamp;
		this.statesCarExteriorState = statesCarExteriorState;
		this.statesCarInteriorState = statesCarInteriorState;
	}

	
	
	/**
	 *	Getter & Setter 
	 *  __________________________________________
	 */
	
	public double getStatesLatitudeCoordinates() {
		return statesLatitudeCoordinates;
	}


	public double getStatesLongitudeCoordinates() {
		return statesLongitudeCoordinates;
	}


	public Timestamp getStatesTimestamp() {
		return statesTimestamp;
	}


	public int getStatesCarFuel() {
		return statesCarFuel;
	}


	public String getStatesCarInteriorState() {
		return statesCarInteriorState;
	}


	public String getStatesCarExteriorState() {
		return statesCarExteriorState;
	}


	public int getStatesParkingSlotsUsed() {
		return statesParkingSlotsUsed;
	}


	public void setStatesLatitudeCoordinates(double statesLatitudeCoordinates) {
		this.statesLatitudeCoordinates = statesLatitudeCoordinates;
	}


	public void setStatesLongitudeCoordinates(double statesLongitudeCoordinates) {
		this.statesLongitudeCoordinates = statesLongitudeCoordinates;
	}


	public void setStatesTimestamp(Timestamp statesTimestamp) {
		this.statesTimestamp = statesTimestamp;
	}


	public void setStatesCarFuel(int statesCarFuel) {
		this.statesCarFuel = statesCarFuel;
	}


	public void setStatesCarInteriorState(String statesCarInteriorState) {
		this.statesCarInteriorState = statesCarInteriorState;
	}


	public void setStatesCarExteriorState(String statesCarExteriorState) {
		this.statesCarExteriorState = statesCarExteriorState;
	}


	public void setStatesParkingSlotsUsed(int statesParkingSlotsUsed) {
		this.statesParkingSlotsUsed = statesParkingSlotsUsed;
	}
	
	public static boolean hasPairOfStatesSameCoordinates(State firstState, State secondState){
		if(null!=firstState && null!=secondState){
			if(firstState.getStatesLatitudeCoordinates()==secondState.getStatesLatitudeCoordinates() &&
					secondState.getStatesLongitudeCoordinates()==secondState.getStatesLongitudeCoordinates())
						return true;
		}
		return false;
	}
}
