package utilities;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import database_model.State;
import web_crawler_model.Coordinate;
import web_crawler_model.Location;

/**
 * This class contains helper-methods, which do things like
 * converting dates in other formats or calculating coordinates. 
 * 
 * 
 * @author niklasrappel
 */
public class Utilities {
	
	/**
	 * This method converts a java-timestamp
	 * to a CartoDB-valid date-format.
	 * 
	 * Therefore java´s Calendar and Date libraries are used.
	 * 
	 * @param tmpState:		Current state to convert.
	 * @return:				CartoDB-Date as a String-Value
	 */
	public static String getCartoDbDateByState(State tmpState) {
		Calendar cal = Calendar.getInstance();
		Date tmpDate = new Date(tmpState.getStatesTimestamp().getTime());

		cal.setTime(tmpDate);
		String cartodbDate = cal.get(Calendar.YEAR) 
							+ "-" + cal.get(Calendar.MONTH) + 1 //The java-calendar-month starts with 0;
							+ "-" + cal.get(Calendar.DAY_OF_MONTH)  
							+ "T" + cal.get(Calendar.HOUR_OF_DAY)
							+ ":" + cal.get(Calendar.MINUTE)
							+ ":" + cal.get(Calendar.SECOND)
							+ "Z";	        				
		
		System.out.println("CartoDB-Date: " + cartodbDate);
		return cartodbDate;
	}
	
	
	/**
	 * This method is used to receive a location object for a given Coordinate
	 * and location list.
	 * 
	 * @param locations:		List of locations.
	 * @param coordinate:		Respective coordinate.
	 *
	 * @return calculated location-object
	 */
	public static Location getLocationByCoordinates(List<Location> locations, Coordinate coordinate) {
		for (int i = 0; i < locations.size(); i++) {
			if (locations.get(i).getUpperLeft().getCoordinate().getLatitude() > coordinate.getLatitude() 
					&& locations.get(i).getUpperLeft().getCoordinate().getLongitude() < coordinate.getLongitude()
					&& locations.get(i).getLowerRight().getCoordinate().getLatitude() < coordinate.getLatitude()
					&& locations.get(i).getLowerRight().getCoordinate().getLongitude() > coordinate.getLongitude()) {
				return locations.get(i);
			}
		}

		return null;
	}
	
	
	/**
	 * This function proofs if a date is valid or not by parameters year, month and day.
	 * 
	 * @param year:		Has to be >2000;
	 * @param month:	Has to be >0 and <13 (January - December); 
	 * @param day:		Has to be >0 and <32;
	 * 
	 * @return true, if all conditions are valid. 
	 */
	public static boolean isValidDate(int year, int month, int day){
    	if(year > 2000 && month < 13 && month > 0 && day > 0 && day < 32)
    		return true;
    	
    	return false;	
	}
	
	
	/**
	 * This method creates a PostgreSQL-Query that returns a CSV-File by given
	 * output-path, database-table-name, table-column-id and date-time.
	 *  
	 * @param tableName:			Table-Name (e.g. 'state', or 'car')
	 * @param outputPath:			Output-Path on client-system (e.g. /System/User/root/)
	 * @param columnIdentifiers:    Column-ID (e.g. 'car_plate')
	 * @param timestamp:			Given timestamp (e.g. taken out of analysis-time)
	 * @param sdf:					Simple-Date-Format-Object
	 * @return
	 */
	public static String createPostgreSQLCsvOutputPathString(String tableName, String outputPath, String columnIdentifiers, Timestamp timestamp, SimpleDateFormat sdf) {
		return 		"COPY (SELECT " + columnIdentifiers + " FROM " + tableName 
				  + ") TO '" + outputPath 
				  + "/" + tableName + "_" + sdf.format(timestamp) + ".csv" 
				  +"' WITH CSV;";
	}
}
