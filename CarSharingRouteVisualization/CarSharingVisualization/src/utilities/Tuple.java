package utilities;


/**
 * This class allows us to shape tuple-objects in java.
 * Tuple.java allows generating generic tuple-objects. 
 * 
 * @author niklasrappel
 *
 * @param <S>
 * @param <T>
 */
public class Tuple<S, T> {
    
	private final S firstValue;
    private final T secondValue;

    
    /**
     * Initial-Constructor 
     * 
     * @param firstValue
     * @param secondValue
     */
    public Tuple(S firstValue, T secondValue) { 
        this.firstValue = firstValue;
        this.secondValue = secondValue;
    }


    
    /**
     * GETTER/SETTER
     * 
     */
	public S getFirstValue() {
		return firstValue;
	}


	public T getSecondValue() {
		return secondValue;
	}
}
