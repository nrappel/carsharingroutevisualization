<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Fervency 
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20131006

-->
<%@page import="database_handler.DatabaseHandler"%>
<%@page import="java.sql.*"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href="CSS/default.css" rel="stylesheet" type="text/css" media="all" />
<link href="CSS/fonts.css" rel="stylesheet" type="text/css" media="all" />

<script src="/CarSharingVisualization/js/jquery-min.js"></script>
<script src="/CarSharingVisualization/js/jquery-ui.js"></script>


<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->

<% 
	Connection connection = DatabaseHandler.establishDatabaseConnection();
%>


<script>
$(function(){
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
    
        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');
    
            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
    
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
    
            // Replace image with new SVG
            $img.replaceWith($svg);
    
        }, 'xml');
    
    });
});
</script>


<script>
	
	$(document).ready(function(){
		$("a[href^=#]").click(function(e) { 
			e.preventDefault(); 
			var dest = $(this).attr('href'); 
			console.log(dest); 
			$('html,body').animate({ 
				scrollTop: $(dest).offset().top 
			}, 'slow'); 
		});
	});
	
</script>


</head>
<body>
<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<div>
				<img class="svg" src="images/maki/car-24.svg" />
			</div>
			<h1><a href="#">Visualization of car sharing routes</a></h1>
			<span>Analysis by Niklas Rappel</span> </div>
		<div id="menu">
			<ul>
				<li class="current_page_item"><a id="aboutSectionLinkId" href="#aboutSectionId" accesskey="1" title="">About the experiment</a></li>
				<li><a href="#trackACarAnalysisSectionId" accesskey="2" title="">Analysis: Track a car</a></li>
				<li><a href="#carSharingTrafficAnalysisId" accesskey="3" title="">Analysis: Car-sharing city-district-traffic</a></li>
			</ul>
		</div>
	</div>
</div>

<div id="wrapper3">
	<div id="portfolio" class="container">
		<div id="aboutSectionId" class="title">
			<h2>About the experiment</h2>
			<span class="byline">Integer sit amet pede vel arcu aliquet pretium</span> </div>
		<div class="column1">
			<div class="box"> <a href="#"><img src="images/scr01.jpg" alt="" class="image image-full" /></a>
				<h3>Vestibulum venenatis</h3>
				<p>Fermentum nibh augue praesent a lacus at urna congue rutrum.</p>
				<a href="#" class="button button-small">Etiam posuere</a> </div>
		</div>
		<div class="column2">
			<div class="box"> <a href="#"><img src="images/scr02.jpg" alt="" class="image image-full" /></a>
				<h3>Praesent scelerisque</h3>
				<p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
				<a href="#" class="button button-small">Etiam posuere</a> </div>
		</div>
		<div class="column3">
			<div class="box"> <a href="#"><img src="images/scr03.jpg" alt="" class="image image-full" /></a>
				<h3>Donec dictum metus</h3>
				<p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
				<a href="#" class="button button-small">Etiam posuere</a> </div>
		</div>
		<div class="column4">
			<div class="box"> <a href="#"><img src="images/scr04.jpg" alt="" class="image image-full" /></a>
				<h3>Mauris vulputate dolor</h3>
				<p>Rutrum fermentum nibh in augue praesent urna congue rutrum.</p>
				<a href="#" class="button button-small">Etiam posuere</a> </div>
		</div>
	</div>
</div>

	<form action="CarTrackerByDate" method="post">  

<div id="wrapper1">
	<div id="welcome" class="container">
		<div id="trackACarAnalysisSectionId" class="title">
			<h2>Analysis: Track a car</h2>
			<span 
				class="byline">Mauris vulputate dolor sit amet nibh
			</span> 
		</div>
		<div class="content">
			<select name="carSelector" id="carSelector"> 
			 	<option value="392015">ALL CARS</option>
			  	<%
				PreparedStatement psmnt = connection.prepareStatement("SELECT car_plate FROM car;");
				ResultSet rs2 = psmnt.executeQuery();
				while(rs2.next()){
					String carPlateString = rs2.getString("car_plate");
					
				%>
				<option value="<%= carPlateString %>"><%=carPlateString%></option>
				<%
				} 
				%>
			</select>
				
			<button name="submitTrackCarAnalysisButtonName" id="submitTrackCarAnalysisButtonId" class="submit_button" type="submit">START-ANALYSIS!</button>	
		</div>
	</div>
</div>

<div id="wrapper2">
	<div id="welcome" class="container">
		<div id="carSharingTrafficAnalysisId" class="title">
			<h2>Analysis: Car-sharing city-district-traffic</h2>
			<span 
				class="byline">Mauris vulputate dolor sit amet nibh
			</span> 
		</div>
		<div class="content">
			
			<button name="submitCarSharingTrafficAnalysisButtonName" id="submitCarSharingTrafficAnalysisButtonId" class="submit_button" type="submit">START-DISTRICT-ANALYSIS!</button>	
		</div>
	</div>
</div>

</form>


<div id="wrapper3">
	<div id="newsletter" class="container">
		<div class="title">
			<h2>Contact:</h2>
			<span class="byline">Integer sit amet pede vel arcu aliquet pretium</span> </div>
		<div class="content">
			<form method="post" action="#">
				<div class="row half">
					<div class="6u">
						<input type="text" class="text" name="name" placeholder="Name" />
					</div>
					<div class="6u">
						<input type="text" class="text" name="email" placeholder="Email" />
					</div>
				</div>
				<div class="row half">
					<div class="12u">
						<textarea name="message" placeholder="Message"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="12u"> <a href="#" class="button submit">Send Message</a> </div>
				</div>
			</form>
		</div>
	</div>
</div>


</body>
</html>
