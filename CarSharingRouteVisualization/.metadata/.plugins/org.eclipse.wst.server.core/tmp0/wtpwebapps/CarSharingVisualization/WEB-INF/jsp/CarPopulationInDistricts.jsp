

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="database_model.Tuple"%>
<%@page import="java.util.LinkedList"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.js'></script>
<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-pip/v0.0.2/leaflet-pip.js'></script>
<script src='https://code.jquery.com/jquery-1.11.0.min.js'></script>
<script> src="https://raw.github.com/tmcw/csv2geojson/gh-pages/csv2geojson.js"</script>
<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<script src='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-omnivore/v0.2.0/leaflet-omnivore.min.js'></script>
<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-image/v0.0.4/leaflet-image.js'></script>
<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-draw/v0.2.2/leaflet.draw.js'></script>
<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-geodesy/v0.1.0/leaflet-geodesy.js'></script>

<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.5/mapbox.css' rel='stylesheet' />
<link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-draw/v0.2.2/leaflet.draw.css' rel='stylesheet' />


<style>
  body { margin:0; padding:0; }

  
  #map { position:absolute; top:0; bottom:0; width:100%; }
  .state {
  position:absolute;
  bottom:100px;
  right:10px;
  z-index:1000;
  }
  
  .leaflet-popup-content {
    width:320px !important;
  }
  
  .leaflet-popup-content img {
    align: center;
  }
  
  .popup_cardata_table{
  	margin: 0px auto;
  }
  
  .column_content_class {
  	text-align: center;
  }
  
  .state strong {
    background:#CC0000;
    color:#fff;
    display:block;
    padding:10px;
    border-radius:2px;
    }
    .ui-button {
  position:absolute;
top:10px;
right:200px;
z-index:1000;
  }

#snapshot {
  position:absolute;
  top:0;bottom:0;right:0;
  width:50%;
  }
</style>

<link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
<link href='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />


</head>

<body>
	<script src='https://api.tiles.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
	<pre id='coordinates' class='ui-coordinates'></pre>
	

	<div id='map'></div>
	<div id='state' class='state'></div>
	<button id='snap' name="getDistrictPopulationButtonName" class='ui-button'>Print districts</button>
	
	<script>
		// Provide your access token
		L.mapbox.accessToken = 'pk.eyJ1IjoibnJhcHBlbCIsImEiOiJVa1h0WVljIn0.SYGvzm-95iyOf0CwF706Gw';

		var geocoder = L.mapbox.geocoder('mapbox.places');
		var state = document.getElementById('state');
		var snapshot = document.getElementById('snapshot');
		
		var map = L.mapbox.map('map', 'nrappel.lbpp2n8n');
		var myLayer = L.mapbox.featureLayer().addTo(map);

		
		
		
		
		geocoder.query('Berlin', showMap);
		
		
		var states = null;
		// DISTRICTS 
		$.ajax({
		    url: 'geojson_files/berliner-bezirke.geojson',
		    dataType: 'json',
		    success: function load(d) {
		        states = L.geoJson(d).addTo(map);
		        L.marker([52.5, 13.3], {
		            icon: L.mapbox.marker.icon({
		                'marker-color': '#f86767'
		            }),
		            draggable: true
		        }).addTo(map)
		        .on('dragend', function(e) {
		            var layer = leafletPip.pointInLayer(this.getLatLng(), states, true);
		            if (layer.length) {
		              state.innerHTML = '<strong>' + layer[0].feature.properties.Name + '</strong>';
		            } else {
		              state.innerHTML = '';
		            }
		        });
		    }
		});
		
		
		
		
		
		omnivore.csv('csv_files/csv_public_transportation/stops.csv').on('ready', function(layer) {
		    
	        this.eachLayer(function(marker) {	            

	            var properties =  marker.toGeoJSON().properties;
	            var latLng = marker.getLatLng();	            
	            var myLayer = null;
	            
	            if(states !== null){
	            	myLayer = leafletPip.pointInLayer(latLng, states, true);
	            
	            	if(myLayer[0] !== null && myLayer[0] !== undefined && myLayer[0].feature !== null && myLayer[0].feature !== undefined){
			            districtArray.push(myLayer[0].feature.properties.Name);
			            
			            marker.bindPopup(	
			            	'<img src="images/ubahn_logo.png" />'
						);
		                			            
			            var icon = L.mapbox.marker.icon({
	            			'marker-size': 'small',
	    	                'marker-symbol': 'bus',
			                'marker-color': '#2B3856'
		                });
	            		marker.setIcon(icon);
			           
	            	}
	            };
	        });
	    }).addTo(map);
		
		
		
		
		
		
		
		var districtArray = [];
		var markers = new L.MarkerClusterGroup();
		
		omnivore.csv('csv_files/csv_state_analysis/392015.csv').on('ready', function(layer) {
	        // An example of customizing marker styles based on an attribute.
	        // In this case, the data, a CSV file, has a column called 'state'
	        // with values referring to states. Your data might have different
	        // values, so adjust to fit.
	        this.eachLayer(function(marker) {	            
	            // Bind a popup to each icon based on the same properties

	            var properties =  marker.toGeoJSON().properties;
	            var latLng = marker.getLatLng();
	            

	            var myLayer = null;
	            
	            if(states !== null){
	            	myLayer = leafletPip.pointInLayer(latLng, states, true);
	            
	            	if(myLayer[0] !== null && myLayer[0] !== undefined && myLayer[0].feature !== null && myLayer[0].feature !== undefined){
			            districtArray.push(myLayer[0].feature.properties.Name);
			            
			            var districtName = myLayer[0].feature.properties.Name;
			            marker.bindPopup(	
			            		'<img src="images/car2go_logo.png" />'
								+ '</br></br>' + 
								'<table class="popup_cardata_table">' + 
			            		'<tr><td><b>Plate:</td><td class="column_content_class">' 				+ properties.car_plate 			+ '</b></td></tr>' + 
			            		'<tr><td><b>Fuel:</b></td><td class="column_content_class">' 			+ properties.fuel 				+ '</td></tr>' +
			            		'<tr><td><b>Date/Time:</b></td><td class="column_content_class">' 		+ properties.date  				+ '</td></tr>' + 
			            		'<tr><td><b>Interior-State:</b></td><td class="column_content_class">' 	+ properties.interior_state  	+ '</td></tr>' +
			            		'<tr><td><b>Exterior-State:</b></td><td class="column_content_class">' 	+ properties.exterior_state 	+ '</td></tr>' + 
			            		'<tr><td><b>Parking-Slots:</b></td><td class="column_content_class">' 	+ properties.free_parking_slots + '</td></tr>' +
			            		'<tr><td><b>Latitude:</b></td><td class="column_content_class">' 		+ latLng.lat  					+ '</td></tr>' +
			            		'<tr><td><b>Longitude:</b></td><td class="column_content_class">' 		+ latLng.lng  					+ '</td></tr>' + 
			            		'<tr><td><b>District:</b></td><td class="column_content_class">' 		+ districtName  				+ '</td></tr>' +
			            		'</table>');
		            
			            
			            markers.addLayer(marker);
			            
			            switch(districtName){
			            	case "Steglitz-Zehlendorf":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#2B3856'
				                });
			            		marker.setIcon(icon);
			            		break;
			            	case "Spandau":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#357EC7'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Reinickendorf":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#348017'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Charlottenburg-Wilmersdorf":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#FFF380'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Mitte":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#DEB887'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Pankow":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#493D26'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Friedrichshain-Kreuzberg":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#6F4E37'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Neuk�lln":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#C35817'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Tempelhof-Sch�neberg":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#E67451'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Treptow-K�penick":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#E55451'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Marzahn-Hellersdorf":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#810541'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            	case "Lichtenberg":
			            		var icon = L.mapbox.marker.icon({
			            			'marker-size': 'large',
			    	                'marker-symbol': 'car',
					                'marker-color': '#F6358A'
				                });
			            		marker.setIcon(icon);
			            		
			            		break;
			            }
	            }
	            
				
	            

		            
	            };
	            
	        });
	    })
	    //TODO reinsert to show all markers in map!
	    //.addTo(map)
	    ;
		
		
		map.addLayer(markers);

		
		

		
		
		
		
		
		
		
		
		
		
		
		//POI: Reichstag
		var reichstagDivNode = document.createElement('DIV');
		reichstagDivNode.innerHTML = '<img src="images/reichstag.jpg" />'
							+ '</br>'
							+ '</br><b>Reichstag</b>'
							+ '</br><b>Adresse:</b> Platz der Republik 1, 11011 Berlin';
		
		L.marker([52.518623, 13.376198], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'town-hall',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(reichstagDivNode);
		
		 
		//POI: Brandenburger Tor
		var brandenburgerTorDivNode = document.createElement('DIV');
		brandenburgerTorDivNode.innerHTML = '<img src="images/brandenburger_tor.jpg" />'
							+ '</br>'
							+ '</br><b>Brandenburger Tor</b>'
							+ '</br><b>Adresse:</b> Pariser Platz, 10117 Berlin';
		
		L.marker([52.5162209, 13.37782859999993], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'town-hall',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(brandenburgerTorDivNode);
		
		
		//POI: Fernsehturm
		var fernsehturmDivNode = document.createElement('DIV');
		fernsehturmDivNode.innerHTML = '<img src="images/fernsehturm.jpg" />'
							+ '</br>'
							+ '</br><b>Fernsehturm</b>'
							+ '</br><b>Adresse:</b> Panoramastr. 1A, 10178 Berlin';
		
		L.marker([52.520645, 13.409779], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'lighthouse',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(fernsehturmDivNode);
		
		
		//POI: Gendarmenmarkt
		var gendarmenmarktDivNode = document.createElement('DIV');
		gendarmenmarktDivNode.innerHTML = '<img src="images/gendarmenmarkt.jpg" />'
							+ '</br>'
							+ '</br><b>Gendarmenmarkt</b>'
							+ '</br><b>Adresse:</b> Gendarmenmarkt, 10117 Berlin';
		
		L.marker([52.52944, 13.37973], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'star',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(gendarmenmarktDivNode);
		
		
		//POI: Dom
		var domDivNode = document.createElement('DIV');
		domDivNode.innerHTML = '<img src="images/dom.jpg" />'
							+ '</br>'
							+ '</br><b>Dom</b>'
							+ '</br><b>Adresse:</b> Am Lustgarten, 10178 Berlin';
		
		L.marker([52.51914, 13.401], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'religious-christian',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(domDivNode);
		
		
		//POI: Kurfuerstendamm
		var kurfuerstendammDivNode = document.createElement('DIV');
		kurfuerstendammDivNode.innerHTML = '<img src="images/kurfuerstendamm.jpg" />'
							+ '</br>'
							+ '</br><b>Kurfuerstendamm</b>'
							+ '</br><b>Adresse:</b> Kurfuerstendamm, 14059 Berlin';
		
		L.marker([52.50030550, 13.307675600000039], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'town',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(kurfuerstendammDivNode);
		
		
		//POI: Schloss Charlottenburg
		var schlossCharlottenburgDivNode = document.createElement('DIV');
		schlossCharlottenburgDivNode.innerHTML = '<img src="images/schloss_charlottenburg.jpg" />'
							+ '</br>'
							+ '</br><b>Schloss Charlottenburg</b>'
							+ '</br><b>Adresse:</b> Spandauer Damm 20-24, 14059 Berlin';
		
		L.marker([52.5199, 13.2962], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'town-hall',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(schlossCharlottenburgDivNode);
		
		
		//POI: Siegessaeule
		var siegessaeuleDivNode = document.createElement('DIV');
		siegessaeuleDivNode.innerHTML = '<img src="images/siegessaeule.jpg" />'
							+ '</br>'
							+ '</br><b>Siegessaeule</b>'
							+ '</br><b>Adresse:</b> Gro�er Stern, 10557 Berlin'
							+ '</br>Die Siegess�ule auf dem Gro�en Stern inmitten des Gro�en Tiergartens in Berlin wurde von 1864 bis 1873 als Nationaldenkmal der Einigungskriege nach einem Entwurf von Heinrich Strack erbaut. Sie steht unter Denkmalschutz.';
		
		L.marker([52.51453637761834, 13.350083827972412], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'lighthouse',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(siegessaeuleDivNode);
		
		
		//POI: Bundeskanzleramt
		var bundeskanzleramtDivNode = document.createElement('DIV');
		bundeskanzleramtDivNode.innerHTML = '<img src="images/bundeskanzleramt.jpg" />'
							+ '</br>'
							+ '</br><b>Bundeskanzleramt</b>'
							+ '</br><b>Adresse:</b> Willy-Brandt-Stra�e 1, 10557 Berlin';
		
		L.marker([52.5202294204875, 13.369138240814209], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'town-hall',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(bundeskanzleramtDivNode);
		
		
		//POI: Flughafen Schoenefeld
		var flughafenSchoenefeldDivNode = document.createElement('DIV');
		flughafenSchoenefeldDivNode.innerHTML = '<img src="images/flughafen_schoenefeld.jpg" />'
							+ '</br>'
							+ '</br><b>Flughafen Schoenefeld</b>'
							+ '</br><b>Adresse:</b> Flughafen Schoenefeld, 12521 Berlin';
		
		L.marker([52.38873605471769, 13.51993203163147], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'airport',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(flughafenSchoenefeldDivNode);
		
		
		//POI: Humbold University of Berlin
		var humboldUniversityDivNode = document.createElement('DIV');
		humboldUniversityDivNode.innerHTML = '<img src="images/humbold_university.jpg" />'
							+ '</br>'
							+ '</br><b>Humboldt University of Berlin</b>'
							+ '</br><b>Adresse:</b> Unter den Linden 6, 10099 Berlin';
		
		L.marker([52.518081558628154, 13.393428325653076], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'college',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(humboldUniversityDivNode);
		
		
		//POI: Hauptbahnhof
		var hauptbahnhofDivNode = document.createElement('DIV');
		hauptbahnhofDivNode.innerHTML = '<img src="images/hauptbahnhof.jpg" />'
							+ '</br>'
							+ '</br><b>Hauptbahnhof</b>'
							+ '</br><b>Adresse:</b> Europaplatz 1, 10557 Berlin';
		
		L.marker([52.525007873203286, 13.369385004043579], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'rail',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(hauptbahnhofDivNode);
		
		
		//POI: Berliner Zoo
		var berlinerZooDivNode = document.createElement('DIV');
		berlinerZooDivNode.innerHTML = '<img src="images/berliner_zoo.jpg" />'
							+ '</br>'
							+ '</br><b>Berliner Zoo</b>'
							+ '</br><b>Adresse:</b> Hardenbergplatz 8, 10787 Berlin';
		
		L.marker([52.510016346708475, 13.3375284075737], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'zoo',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(berlinerZooDivNode);
		
		
		//POI: Museum fuer Naturkunde Berlin
		var naturkundeMuseumDivNode = document.createElement('DIV');
		naturkundeMuseumDivNode.innerHTML = '<img src="images/naturkundemuseum.jpg" />'
							+ '</br>'
							+ '</br><b>Museum fuer Naturkunde Berlin</b>'
							+ '</br><b>Adresse:</b> Invalidenstra�e 43, 10115 Berlin';
		
		L.marker([52.53007298773705, 13.379373550415039], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'art-gallery',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(naturkundeMuseumDivNode);
		
		
		//POI: Technical University of Berlin
		var technicalUniversityDivNode = document.createElement('DIV');
		technicalUniversityDivNode.innerHTML = '<img src="images/technische_universitaet.png" />'
							+ '</br>'
							+ '</br><b>Technische Universitaet von Berlin</b>'
							+ '</br><b>Adresse:</b> Stra�e des 17. Juni 135, 10623 Berlin';
		
		L.marker([52.512230, 13.327135], {
			icon: L.mapbox.marker.icon({
	            'marker-color': '#810541',
	            'marker-symbol': 'college',
	            'marker-size': 'large'
	        })    
		}).addTo(map).bindPopup(technicalUniversityDivNode);
		
		
		
		
		// Create a Foursquare developer account: https://developer.foursquare.com/
		// NOTE: CHANGE THESE VALUES TO YOUR OWN:
		// Otherwise they can be cycled or deactivated with zero notice.
		var CLIENT_ID = 'L4UK14EMS0MCEZOVVUYX2UO5ULFHJN3EHOFVQFSW0Z1MSFSR';
		var CLIENT_SECRET = 'YKJB0JRFDPPSGTHALFOEP5O1NDDATHKQ2IZ5RO2GOX452SFA';

		// https://developer.foursquare.com/start/search
		var API_ENDPOINT = 'https://api.foursquare.com/v2/venues/search' +
		  '?client_id=CLIENT_ID' +
		  '&client_secret=CLIENT_SECRET' +
		  '&v=20130815' +
		  '&near=Berlin' +
		  '&query=Tankstelle' +
		  '&callback=?';

		// Keep our place markers organized in a nice group.
		var foursquarePlaces = L.layerGroup().addTo(map);

		// Use jQuery to make an AJAX request to Foursquare to load markers data.
		$.getJSON(API_ENDPOINT
		    .replace('CLIENT_ID', CLIENT_ID)
		    .replace('CLIENT_SECRET', CLIENT_SECRET), function(result, status) {

		    if (status !== 'success') return alert('Request to Foursquare failed');

		    // Transform each venue result into a marker on the map.
		    for (var i = 0; i < result.response.venues.length; i++) {
		      var venue = result.response.venues[i];
		      var latlng = L.latLng(venue.location.lat, venue.location.lng);
		      var marker = L.marker(latlng, {
		          icon: L.mapbox.marker.icon({
		            'marker-color': '#C80000',
		            'marker-symbol': 'fuel',
		            'marker-size': 'medium'
		          })
		        })
		      .bindPopup('<img src="images/petrol_station.png" /></br></br>'
		    		  +  '<b>Tankstelle: <a href="https://foursquare.com/v/' + venue.id + '">' + venue.name + '</a>')
		      .addTo(foursquarePlaces);
		    }

		});
		
		
		
		var API_ENDPOINT_PARKING = 'https://api.foursquare.com/v2/venues/search' +
		  '?client_id=CLIENT_ID' +
		  '&client_secret=CLIENT_SECRET' +
		  '&v=20130815' +
		  '&near=Berlin' +
		  '&query=Parkhaus Parkplatz' +
		  '&callback=?';

		// Keep our place markers organized in a nice group.
		var foursquarePlacesUbahn = L.layerGroup().addTo(map);

		// Use jQuery to make an AJAX request to Foursquare to load markers data.
		$.getJSON(API_ENDPOINT_PARKING
		    .replace('CLIENT_ID', CLIENT_ID)
		    .replace('CLIENT_SECRET', CLIENT_SECRET), function(result, status) {

		    if (status !== 'success') return alert('Request to Foursquare failed');

		    // Transform each venue result into a marker on the map.
		    for (var i = 0; i < result.response.venues.length; i++) {
		      var venue = result.response.venues[i];
		      var latlng = L.latLng(venue.location.lat, venue.location.lng);
		      var marker = L.marker(latlng, {
		          icon: L.mapbox.marker.icon({
		            'marker-color': '#0000A0',
		            'marker-symbol': 'parking-garage',
		            'marker-size': 'medium'
		          })
		        })
		      .addTo(foursquarePlaces);
		    }

		});
		
		
		
		
		
		
		
		
		//PRINT DISTRICTS
		document.getElementById('snap').addEventListener('click', function() {
			var allDistrictsInBerlin = ["Steglitz-Zehlendorf", 
			                            "Spandau", 
			                            "Reinickendorf",
			                            "Charlottenburg-Wilmersdorf",
			                            "Mitte",
			                            "Pankow",
			                            "Lichtenberg",
			                            "Friedrichshain-Kreuzberg",
			                            "Tempelhof-Sch�neberg",
			                            "Neuk�lln",
			                            "Treptow-K�penick",
			                            "Marzahn-Hellersdorf",
			                            "Lichtenberg"
			];
			
			var jsonArr = {};
			
			for(var i=0; i<allDistrictsInBerlin.length; i++) {
			    var count = 0;
			    for(var j=0; j<districtArray.length; j++) {
			        if(allDistrictsInBerlin[i] == districtArray[j])
			        	count++;
			        
			    }
			   
			    jsonArr[allDistrictsInBerlin[i]] = count;			     
			    console.log(allDistrictsInBerlin[i] + ": " + count);
			}
			
			$(document).ready(function() {                   
		    	$.post("", JSON.stringify(jsonArr));
            });
		});
		
		
		//POLYGON-DRAW
		var featureGroup = L.featureGroup().addTo(map);

		var drawControl = new L.Control.Draw({
		  edit: {
		    featureGroup: featureGroup
		  },
		  draw: {
		    polygon: true,
		    polyline: false,
		    rectangle: false,
		    circle: false,
		    marker: false
		  }
		}).addTo(map);
		
		map.on('draw:created', showPolygonArea);
		map.on('draw:edited', showPolygonAreaEdited);
		
		function showPolygonAreaEdited(e) {
		  e.layers.eachLayer(function(layer) {
		    showPolygonArea({ layer: layer });
		  });
		}
		function showPolygonArea(e) {
		  featureGroup.clearLayers();
		  featureGroup.addLayer(e.layer);
		  e.layer.bindPopup((LGeo.area(e.layer) / 1000000).toFixed(2) + ' km<sup>2</sup>');
		  e.layer.openPopup();
		}
		
		
		
		
		//SEVERAL LAYERS
		var layers = {
		      Streets: L.mapbox.tileLayer('examples.map-i87786ca'),
		      Outdoors: L.mapbox.tileLayer('examples.ik7djhcc'),
		      Satellite: L.mapbox.tileLayer('examples.map-igb471ik')
		  };
		
		  layers.Streets.addTo(map);
		  L.control.layers(layers).addTo(map);
				
				
				
			
		function showMap(err, data) {
		    // The geocoder can return an area, like a city, or a
		    // point, like an address. Here we handle both cases,
		    // by fitting the map bounds to an area or zooming to a point.
		    if (data.lbounds) {
		        map.fitBounds(data.lbounds);
		    } else if (data.latlng) {
		        map.setView([data.latlng[0], data.latlng[1]], 13);
		    }
		}
		
	</script>
	
 
</body>

</html>